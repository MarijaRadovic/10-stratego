#include "../Headers/tablesetupscene.h"
#include <iostream>
#include "../Headers/figurine.h"
#include <vector>
#include <QGraphicsScene>
#include <QPoint>
#include <QMouseEvent>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>
#include "../Headers/tablefigurinescene.h"
#include "../Headers/field.h"

int oldI, oldJ;

TableSetupScene::TableSetupScene(QObject *o)
{
    this->setParent(o);
    m_clickedFigurine = nullptr;
    m_otherScene = nullptr;
    m_view = nullptr;
    m_board = nullptr;
    m_squareSize = 120;
}

TableSetupScene::~TableSetupScene()
{
//    delete m_clickedFigurine;
//    delete m_otherScene;
//    delete m_view;
//    delete m_board;
}

void TableSetupScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent){
    if (mouseEvent->button() == Qt::LeftButton)
    {
        int j = ((mouseEvent->scenePos().x()) / m_squareSize);
        int i = ((mouseEvent->scenePos().y()) / m_squareSize);
        if(i<0 || i>4 || j<0 || j>9)
            return;
        i+=5;
        Figurine *f = ((*m_board->getPboard())[i][j])->getFigurine();
        if(m_otherScene->status == SelectingStatus::SelectedPicked){
            int newXPos = ((mouseEvent->scenePos().x()) / m_squareSize);
            int newYPos = ((mouseEvent->scenePos().y()) / m_squareSize);
            newXPos *= m_squareSize;
            newYPos *= m_squareSize;
            if(!(*m_board->getPboard())[i][j]->getFigurine()){
                m_otherScene->removeItem(m_clickedFigurine);
                m_clickedFigurine->setXPos(newXPos);
                m_clickedFigurine->setYPos(newYPos);
                m_clickedFigurine->setSize(m_squareSize);
                this->addItem(m_clickedFigurine);
                ((*m_board->getPboard())[i][j])->setFigurine(m_clickedFigurine);
                m_otherScene->status = SelectingStatus::SelectedMoved;
                m_clickedFigurine->setSelected(false);
            }
        }
        else if(m_otherScene->status == SelectingStatus::SelectedMoved || (m_otherScene->status == SelectingStatus::SelectedForMoving && f)){
            if(m_otherScene->status == SelectingStatus::SelectedForMoving){
                Figurine *tmp = (*m_board->getPboard())[oldI][oldJ]->getFigurine();
                tmp->setSelected(false);
                this->update(tmp->getXPos(), tmp->getYPos(), m_squareSize, m_squareSize);
            }
            m_clickedFigurine = (*m_board->getPboard())[i][j]->getFigurine();
            if(m_clickedFigurine){
                oldI = i;
                oldJ = j;
                m_clickedFigurine->setSelected(true);
                this->update(m_clickedFigurine->getXPos(), m_clickedFigurine->getYPos(), m_squareSize, m_squareSize);
                m_otherScene->status = SelectingStatus::SelectedForMoving;
            }

        }
        else if(m_otherScene->status == SelectingStatus::SelectedForMoving && !f){
            int newXPos = ((mouseEvent->scenePos().x()) / m_squareSize);
            int newYPos = ((mouseEvent->scenePos().y()) / m_squareSize);
            newXPos *= m_squareSize;
            newYPos *= m_squareSize;
            if(!(*m_board->getPboard())[i][j]->getFigurine()){
                Figurine* fig = (*m_board->getPboard())[oldI][oldJ]->getFigurine();
                int forUpdateX = fig->getXPos();
                int forUpdateY = fig->getYPos();
                (*m_board->getPboard())[oldI][oldJ]->setFigurine(nullptr);
                this->removeItem(m_clickedFigurine);
                this->update(forUpdateX, forUpdateY, m_squareSize, m_squareSize);
                this->addItem(fig);
                fig->setXPos(newXPos);
                fig->setYPos(newYPos);
                (*m_board->getPboard())[i][j]->setFigurine(fig);
                m_clickedFigurine->setSelected(false);
                m_otherScene->status = SelectingStatus::SelectedMoved;
                forUpdateX = fig->getXPos();
                forUpdateY = fig->getYPos();
                this->update(forUpdateX, forUpdateY, m_squareSize, m_squareSize);
            }
        }
    }
}

QGraphicsView *TableSetupScene::getView() const
{
    return m_view;
}

void TableSetupScene::setOtherScene(TableFigurineScene *other){
    m_otherScene = other;
}

void TableSetupScene::setView(QGraphicsView* view){
    m_view = view;
}

Figurine* TableSetupScene::getClickedFigurine(){
    return m_clickedFigurine;
}

void TableSetupScene::SetClickedFigurine(Figurine *f){
    m_clickedFigurine = f;
}

Board* TableSetupScene::getBoard(){
    return m_board;
}
void TableSetupScene::setBoard(Board* board){
    m_board = board;
}
