#include "../Headers/game.h"

int getRandomNumber(int a, int b);

int currdepth = 0;

void randomizeState(State &state, int difficulty){
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10; j++){
            Figurine *f = state[i][j].getFigurine();
            if(f && f->getOwner() == PlayerType::Human && f->getFigurineType() != FigurineType::Flag){
                if(getRandomNumber(1,100) > difficulty){
                    int rnd = getRandomNumber(1, 11);
                    state[i][j].setFigurine(new Figurine(PlayerType::Human, rnd));
                }
            }
        }
    }
}


int Game::getPlayerPonts() const
{
    return playerPonts;
}

void Game::setPlayerPonts(int value)
{
    playerPonts = value;
}

int Game::getOpponentPoints() const
{
    return opponentPoints;
}

void Game::setOpponentPoints(int value)
{
    opponentPoints = value;
}

void Game::updateScoreLabel()
{
    m_statuses->setText(
                "Battle status:\n"
                "You: " + QString::number(playerPonts) + " figurines\n"
                "Opponent: " + QString::number(opponentPoints) + " figurines");
}

Game::Game(Player *player1, Player *player2, Board *board, GameScene *gameScene, QLabel *msg)
{
    m_player1 = player1;
    m_player2 = player2;
    m_board = board;
    m_gameScene = gameScene;
    m_turn = Turn::Human;
    m_msg = msg;
    m_gameEnded = false;
    playerPonts = 40;
    opponentPoints = 40;
}

Game::Game()
{
    m_player1 = nullptr;
    m_player2 = nullptr;
    m_board = nullptr;
    m_gameScene = nullptr;
    m_turn = Turn::Human;
    m_gameEnded = false;
    playerPonts = 40;
    opponentPoints = 40;
}

Game::~Game()
{
//    delete m_player1;
//    delete m_player2;
//    delete m_board;
//    delete m_gameScene;
//    delete m_msg;
//    delete m_notification;
}


void Game::playGame()
{
    if(getGameEnded()){
        return;
    }
    updateScoreLabel();

    srand(time(0));
    getBoard()->fromPointerToState();
    State st;
    copyState(st, getBoard()->getBoard());

    int state_value = evaluateBoard(st);
    checkEvaluation(state_value);

    if(getGameEnded()){
        m_notification->setText("Game over! \nYou won! \nPress QUIT");
        return;
    }

    if(checkNoMoreMoves(PlayerType::Computer, getBoard())){
        m_notification->setText("Game over! \nYou won! \nPress QUIT");
        return;
    }
    if(checkNoMoreMoves(PlayerType::Human, getBoard())){
        m_notification->setText("Game over! \nComputer won! \nPress QUIT");
        return;
    }

    if(getTurn() == Turn::Computer){

        int rnd = getRandomNumber(1, 100);
        bool shouldRandomize = (rnd > getDifficulty()) ? true : false;

        int maxNum = std::numeric_limits<int>::max();
        int minNum = std::numeric_limits<int>::min();

        minMaxParams resultOfMaxi = maxi(1, st, minNum, maxNum, shouldRandomize);



        int oldI = resultOfMaxi.oldI;
        int oldJ = resultOfMaxi.oldJ;
        int newI = resultOfMaxi.newI;
        int newJ = resultOfMaxi.newJ;

        (getBoard()->getBoard())[oldI][oldJ].getFig().setOldI(oldI);
        (getBoard()->getBoard())[oldI][oldJ].getFig().setOldJ(oldJ);
        m_gameScene->moveFigurine(oldI, oldJ, newI, newJ, getBoard(), 90);
        getBoard()->fromPointerToState();
        state_value = evaluateBoard(getBoard()->getBoard());
        checkEvaluation(state_value);
        if(getGameEnded()){
            m_notification->setText("Game over! \nComputer won! \nPress QUIT");
            return;
        }
        if(checkNoMoreMoves(PlayerType::Human, getBoard())){
            m_notification->setText("Game over! \nComputer won! \nPress QUIT");
            return;
        }
        if(checkNoMoreMoves(PlayerType::Computer, getBoard())){
            m_notification->setText("Game over! \nYou won! \nPress QUIT");
            return;
        }
        setTurn(Turn::Human);
        m_notification->setText("It's your turn! \n Make a move!");
    }
}

minMaxParams Game::maxi(int depth, State& state, int alpha, int beta, bool shouldRandomize)
{
    currdepth = depth;
    if(shouldRandomize)
        randomizeState(state, this->getDifficulty());

    int current_evaluation = evaluateBoard(state);

    if(depth == 4 || current_evaluation >= abs(3000)){

        minMaxParams retVal;

        retVal.evaluationScore = current_evaluation;
        retVal.newI = -1; retVal.newJ = -1;
        retVal.oldI = -1; retVal.oldJ = -1;
        return retVal;
    }

    int max_evaluation = std::numeric_limits<int>::min();
    int maxOldI = -1, maxOldJ = -1;
    int maxNewI = -1, maxNewJ = -1;

    int OldI = -1, OldJ = -1;
    int NewI = -1, NewJ = -1;
    std::vector<stateParams> nextStates = getNextStates(state, Turn::Computer);
    minMaxParams retVal;

    for(stateParams nextState : nextStates){
        OldI = nextState.oldI;
        OldJ = nextState.oldJ;
        NewI = nextState.newI;
        NewJ = nextState.newJ;

        minMaxParams miniResult = mini(depth+1, nextState.state, alpha, beta, shouldRandomize);

        if(miniResult.evaluationScore >= max_evaluation){
            max_evaluation = miniResult.evaluationScore;
            maxOldI = OldI;
            maxOldJ = OldJ;
            maxNewI = NewI;
            maxNewJ = NewJ;
        }
        if(max_evaluation >= beta){
            retVal.evaluationScore = max_evaluation;
            retVal.oldI = OldI;
            retVal.oldJ = OldJ;
            retVal.newI = NewI;
            retVal.newJ = NewJ;
            return retVal;
        }
        if(max_evaluation > alpha)
            alpha = max_evaluation;
    }
    retVal.newI = maxNewI;
    retVal.newJ = maxNewJ;
    retVal.oldI = maxOldI;
    retVal.oldJ = maxOldJ;
    retVal.evaluationScore = max_evaluation;
    return retVal;
}

minMaxParams Game::mini(int depth, State& state, int alpha, int beta, bool shouldRandomize)
{
    currdepth = depth;
    int current_evaluation = evaluateBoard(state);

    if(depth == 4 || current_evaluation >= abs(3000)){

        minMaxParams retVal;
        retVal.evaluationScore = current_evaluation;
        retVal.newI = -1; retVal.newJ = -1;
        retVal.oldI = -1; retVal.oldJ = -1;
        return retVal;
    }

    int min_evaluation = std::numeric_limits<int>::max();
    int minOldI = -1, minOldJ = -1;
    int minNewI = -1, minNewJ = -1;

    int OldI = -1, OldJ = -1;
    int NewI = -1, NewJ = -1;
    std::vector<stateParams> nextStates = getNextStates(state, Turn::Human);
    minMaxParams retVal;

    for(stateParams nextState : nextStates){
        OldI = nextState.oldI;
        OldJ = nextState.oldJ;
        NewI = nextState.newI;
        NewJ = nextState.newJ;

        minMaxParams miniResult = maxi(depth+1, nextState.state, alpha, beta, shouldRandomize);

        if(miniResult.evaluationScore <= min_evaluation){
            min_evaluation = miniResult.evaluationScore;
            minOldI = OldI;
            minOldJ = OldJ;
            minNewI = NewI;
            minNewJ = NewJ;
        }
        if(min_evaluation <= alpha){
            retVal.evaluationScore = min_evaluation;
            retVal.oldI = OldI;
            retVal.oldJ = OldJ;
            retVal.newI = NewI;
            retVal.newJ = NewJ;
            return retVal;
        }
        if(min_evaluation < beta)
            beta = min_evaluation;
    }
    retVal.newI = minNewI;
    retVal.newJ = minNewJ;
    retVal.oldI = minOldI;
    retVal.oldJ = minOldJ;
    retVal.evaluationScore = min_evaluation;
    return retVal;
}

State &moveFigurine(int oldI, int oldJ, int i, int j, Figurine* figurine, State& state){
    state[i][j].setFigurine(figurine);
    state[i][j].setFieldType(FieldType::Occupied);

    state[i][j].getFig().setOldI(oldI);
    state[i][j].getFig().setOldJ(oldJ);

    return state;
}

State &deleteFigurine(int i, int j, State& state){
    state[i][j].setFigurine(nullptr);
    state[i][j].setFieldType(FieldType::Free);
    return state;
}

void Game::copyState(State &newState, State &state)
{
    newState.resize(10);
    for(int i=0; i<10; i++)
        newState[i].resize(10,Field(FieldType::Free));

    for(int i=0; i<10; i++){
        for(int j=0; j<10; j++){
            newState[i][j].setFigurine(state[i][j].getFigurine());
            newState[i][j].setXPos(state[i][j].getXPos());
            newState[i][j].setYPos(state[i][j].getYPos());
            newState[i][j].setFieldType(state[i][j].getFieldType());
            if(true){
                const int forSetI = state[i][j].getFig().getOldI();
                const int forSetJ = state[i][j].getFig().getOldJ();
                newState[i][j].getFig().setOldI(forSetI);
                newState[i][j].getFig().setOldJ(forSetJ);
            }
        }
    }
}

void Game::resetStateParams(stateParams &stateP, State st)
{
    copyState(stateP.state, st);
    stateP.oldI = -1;
    stateP.oldJ = -1;
    stateP.newI = -1;
    stateP.newJ = -1;
}

void Game::initializeStateParams(stateParams& sp, State st, int oldI, int oldJ, int newI, int newJ)
{
    copyState(sp.state,st);
    sp.oldI = oldI;
    sp.oldJ = oldJ;
    sp.newI = newI;
    sp.newJ = newJ;
}

std::vector<stateParams> Game::getNextStates(State& st, Turn turn)
{
    int i,j;
    std::vector<stateParams> newStates;

    PlayerType pt;

    if(turn == Turn::Computer)
        pt = PlayerType::Computer;
    else
        pt = PlayerType::Human;

    stateParams sp;
    resetStateParams(sp,st);

    State state;
    copyState(state, st);
    Figurine* enemy;
    Figurine* figurine;

    Figurine fig;

    int k = 1;
    for(i = 0; i<10; i++){
        for(j = 0; j<10; j++){

            if(st[i][j].getFigurine() != nullptr){

                figurine = st[i][j].getFigurine();

                fig = Figurine();
                fig.setOldI(st[i][j].getFig().getOldI());
                fig.setOldJ(st[i][j].getFig().getOldJ());

                if(figurine->getOwner() != pt)
                    continue;

                if(figurine != nullptr){

                    if(figurine->getFigurineType() == FigurineType::Scout){

                        k = 1;
                        while(i+k < 10 && (st[i+k][j].getFigurine() == nullptr) && !st[i+k][j].isForbidden() ){

                            if(fig.getOldI() != i+k || fig.getOldJ() != j)
                            {
                                copyState(state, moveFigurine(i, j, i+k,j,figurine,state));
                                copyState(state, deleteFigurine(i,j,state));
                                initializeStateParams(sp, state, i, j, i+k, j);
                                newStates.push_back(sp);
                                copyState(state,st);
                                resetStateParams(sp,st);
                            }

                            k++;
                        }
                        if(i+k < 10 && (st[i+k][j].getFigurine() != nullptr)){

                            enemy = st[i+k][j].getFigurine();

                            if(enemy->getOwner() != figurine->getOwner()){

                                if(fig.getOldI() != i+k || fig.getOldJ() != j)
                                {

                                    if((*figurine) > (*enemy)){
                                        copyState(state, moveFigurine(i, j, i+k,j,figurine,state));
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i+k, j);
                                        newStates.push_back(sp);
                                        copyState(state,st);
                                        resetStateParams(sp,st);
                                    }
                                    else if((*figurine) == (*enemy)){
                                        copyState(state, deleteFigurine(i+k,j,state));
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i+k, j);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                    else {
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i+k, j);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                }
                            }
                        }

                        k = 1;
                        while(i-k >= 0 && (st[i-k][j].getFigurine() == nullptr) && !st[i-k][j].isForbidden() ){
                            if(fig.getOldI() != i-k || fig.getOldJ() != j)
                            {

                                copyState(state, moveFigurine(i, j, i-k,j,figurine,state));

                                copyState(state, deleteFigurine(i,j,state));
                                initializeStateParams(sp, state, i, j, i-k, j);
                                newStates.push_back(sp);
                                copyState(state,st);
                                resetStateParams(sp,st);
                            }
                            k++;
                        }
                        if(i-k >= 0 && (st[i-k][j].getFigurine() != nullptr) ){

                            enemy = st[i-k][j].getFigurine();

                            if(enemy->getOwner() != figurine->getOwner()){

                                if(fig.getOldI() != i-k || fig.getOldJ() != j)
                                {

                                    if((*figurine) > (*enemy)){
                                        copyState(state, moveFigurine(i, j, i-k,j,figurine,state));
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i-k, j);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                    if((*figurine) == (*enemy)){
                                        copyState(state, deleteFigurine(i-k,j,state));
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i-k, j);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                    else {
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i-k, j);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                }
                            }

                        }

                        k = 1;
                        while(j+k < 10 && (st[i][j+k].getFigurine() == nullptr) && !st[i][j+k].isForbidden() ){
                            if(fig.getOldI() != i || fig.getOldJ() != j+k)
                            {

                                copyState(state, moveFigurine(i, j, i,j+k,figurine,state));

                                copyState(state, deleteFigurine(i,j,state));
                                initializeStateParams(sp, state, i, j, i, j+k);
                                newStates.push_back(sp);
                                copyState(state, st);
                                resetStateParams(sp,st);
                            }
                            k++;
                        }
                        if(j+k < 10 && (st[i][j+k].getFigurine() != nullptr) ){

                            enemy = st[i][j+k].getFigurine();

                            if(enemy->getOwner() != figurine->getOwner()){

                                if(fig.getOldI() != i || fig.getOldJ() != j+k)
                                {

                                    if((*figurine) > (*enemy)){
                                        copyState(state, moveFigurine(i, j, i,j+k,figurine,state));
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i, j+k);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                    if((*figurine) == (*enemy)){
                                        copyState(state, deleteFigurine(i,j+k,state));
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i, j+k);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                    else {
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i, j+k);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                }
                            }
                        }


                        k = 1;
                        while(j-k >= 0 && (st[i][j-k].getFigurine() == nullptr) && !st[i][j-k].isForbidden() ){

                            if(fig.getOldI() != i || fig.getOldJ() != j-k)
                            {
                                copyState(state, moveFigurine(i, j, i,j-k,figurine,state));

                                copyState(state, deleteFigurine(i,j,state));
                                initializeStateParams(sp, state, i, j, i, j-k);
                                newStates.push_back(sp);
                                copyState(state, st);
                                resetStateParams(sp,st);
                            }
                            k++;
                        }
                        if(j-k >= 0 && (st[i][j-k].getFigurine() != nullptr) ){

                            enemy = st[i][j-k].getFigurine();

                            if(enemy->getOwner() != figurine->getOwner()){

                                if(fig.getOldI() != i || fig.getOldJ() != j-k)
                                {

                                    if((*figurine) > (*enemy)){
                                        copyState(state, moveFigurine(i, j, i,j-k,figurine,state));
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i, j-k);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                    if((*figurine) == (*enemy)){
                                        copyState(state, deleteFigurine(i,j-k,state));
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i, j-k);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                    else {
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i, j-k);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                }
                            }
                        }
                    }

                    else if(figurine->getFigurineType() != FigurineType::Bomb && figurine->getFigurineType() != FigurineType::Flag){
                        if(i+1 < 10 && (st[i+1][j].getFigurine() == nullptr) && !st[i+1][j].isForbidden() ){
                            if(fig.getOldI() != i+1 || fig.getOldJ() != j)
                            {
                                copyState(state, moveFigurine(i, j, i+1,j,figurine,state));
                                copyState(state, deleteFigurine(i,j,state));
                                initializeStateParams(sp, state, i, j, i+1, j);
                                newStates.push_back(sp);
                                copyState(state, st);
                                resetStateParams(sp,st);
                            }
                        }

                        if(i+1 < 10 && (st[i+1][j].getFigurine() != nullptr) ){

                            enemy = st[i+1][j].getFigurine();

                            if(enemy->getOwner() != figurine->getOwner()){

                                if(fig.getOldI() != i+1 || fig.getOldJ() != j)
                                {

                                    if((*figurine) > (*enemy)){
                                        copyState(state, moveFigurine(i, j, i+1,j,figurine,state));
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i+1, j);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                    if((*figurine) == (*enemy)){
                                        copyState(state, deleteFigurine(i+1,j,state));
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i+1, j);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                    else {
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i+1, j);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                }
                            }
                        }

                        if(i-1 >= 0 && (st[i-1][j].getFigurine() == nullptr) && !st[i-1][j].isForbidden() ){
                            if(fig.getOldI() != i-1 || fig.getOldJ() != j)
                            {

                                copyState(state, moveFigurine(i, j, i-1,j,figurine,state));

                                copyState(state, deleteFigurine(i,j,state));
                                initializeStateParams(sp, state, i, j, i-1, j);
                                newStates.push_back(sp);
                                copyState(state, st);
                                resetStateParams(sp,st);
                            }
                        }

                        if(i-1 >= 0 && (st[i-1][j].getFigurine() != nullptr) ){

                            enemy = st[i-1][j].getFigurine();

                            if(enemy->getOwner() != figurine->getOwner()){

                                if(fig.getOldI() != i-1 || fig.getOldJ() != j)
                                {

                                    if((*figurine) > (*enemy)){
                                        copyState(state, moveFigurine(i, j, i-1,j,figurine,state));
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i-1, j);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                    if((*figurine) == (*enemy)){
                                        copyState(state, deleteFigurine(i-1,j,state));
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i-1, j);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                    else {
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i-1, j);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                }
                            }
                        }

                        if(j+1 < 10 && (st[i][j+1].getFigurine() == nullptr) && !st[i][j+1].isForbidden() ){
                            if(fig.getOldI() != i || fig.getOldJ() != j+1)
                            {

                                copyState(state, moveFigurine(i, j, i,j+1,figurine,state));
                                copyState(state, deleteFigurine(i,j,state));
                                initializeStateParams(sp, state, i, j, i, j+1);
                                newStates.push_back(sp);
                                copyState(state, st);
                                resetStateParams(sp,st);
                            }
                        }

                        if(j+1 < 10 && (st[i][j+1].getFigurine() != nullptr) ){
                            enemy = st[i][j+1].getFigurine();

                            if(enemy->getOwner() != figurine->getOwner()){

                                if(fig.getOldI() != i || fig.getOldJ() != j+1)
                                {

                                    if((*figurine) > (*enemy)){
                                        copyState(state, moveFigurine(i, j, i,j+1,figurine,state));
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i, j+1);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                    if((*figurine) == (*enemy)){
                                        copyState(state, deleteFigurine(i,j+1,state));
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i, j+1);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                    else {
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i, j+1);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                }
                            }
                        }

                        if(j-1 >= 0 && (st[i][j-1].getFigurine() == nullptr) && !st[i][j-1].isForbidden() ){
                            if(fig.getOldI() != i || fig.getOldJ() != j-1)
                            {
                                copyState(state, moveFigurine(i, j, i,j-1,figurine,state));
                                copyState(state, deleteFigurine(i,j,state));
                                initializeStateParams(sp, state, i, j, i, j-1);
                                newStates.push_back(sp);
                                copyState(state, st);
                                resetStateParams(sp,st);
                            }
                        }

                        if(j-1 >= 0 && (st[i][j-1].getFigurine() != nullptr) ){

                            enemy = st[i][j-1].getFigurine();

                            if(enemy->getOwner() != figurine->getOwner()){

                                if(fig.getOldI() != i || fig.getOldJ() != j-1)
                                {

                                    if((*figurine) > (*enemy)){
                                        copyState(state, moveFigurine(i, j, i,j-1,figurine,state));
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i, j-1);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                    if((*figurine) == (*enemy)){
                                        copyState(state, deleteFigurine(i,j-1,state));
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i, j-1);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                    else {
                                        copyState(state, deleteFigurine(i,j,state));
                                        initializeStateParams(sp, state, i, j, i, j-1);
                                        newStates.push_back(sp);
                                        copyState(state, st);
                                        resetStateParams(sp,st);
                                    }
                                }
                            }
                        }
                    }
                    else
                        continue;
                }
            }
        }
    }

    return newStates;
}


int Game::evaluateBoard(State playingBoard)
{

    int playersEvaluation, computersEvaluation;
    bool playerHasFlag, computerHasFlag;

    std::tuple<int, int,  bool> tmp;
    int playerI = {}, playerJ = {};
    int distCurr, sumDist = 0;

    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10; j++){
            Figurine *f = playingBoard[i][j].getFigurine();
            if(f){
                if(f->getFigurineType() == FigurineType::Flag && f->getOwner() == PlayerType::Human){
                    playerI = i;
                    playerJ = j;
                }
            }
        }
    }

    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10; j++){
            Figurine *f = playingBoard[i][j].getFigurine();
            if(f){
                if(f->getFigurineType()!=FigurineType::Flag && f->getFigurineType()!=FigurineType::Bomb
                        && f->getOwner() == PlayerType::Computer){
                    distCurr = abs(i - playerI) + abs(j - playerJ);
                    sumDist += distCurr;
                }
            }
        }
    }



    //tmp = getPlayer1()->evaluateTable(playingBoard);

    std::tie(playersEvaluation, playerPonts, playerHasFlag) = getPlayer1()->evaluateTable(playingBoard);

    //tmp = getPlayer2()->evaluateTable(playingBoard);

    std::tie(computersEvaluation, opponentPoints, computerHasFlag) = getPlayer2()->evaluateTable(playingBoard);


    if(!playerHasFlag){
        return 10000 - currdepth;
    }
    if(!computerHasFlag){
        return -10000 + currdepth;
    }

    return computersEvaluation - playersEvaluation - sumDist;
}

void Game::drawTable(Board *board, bool firstTime)
{
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10; j++){
            Figurine *figTemp = (*board->getPboard())[i][j]->getFigurine();
            if(figTemp != nullptr){
                (*board->getPboard())[i][j]->setFieldType(FieldType::Occupied);
                if(firstTime)
                    m_gameScene->addItem(figTemp);
                int sqSize = 90;
                int newXpos = sqSize * j;
                int fieldXpos = newXpos;
                int newYpos = sqSize * i;
                int fieldYpos = newYpos;
                (*board->getPboard())[i][j]->setXPos(fieldXpos);
                (*board->getPboard())[i][j]->setYPos(fieldYpos);
                figTemp->setXPos((*board->getPboard())[i][j]->getXPos());
                figTemp->setYPos((*board->getPboard())[i][j]->getYPos());
                figTemp->setSize(sqSize);
            }
        }
    }
}

Player *Game::getPlayer2() const
{
    return m_player2;
}

void Game::setPlayer2(Player *player2)
{
    m_player2 = player2;
}

Player *Game::getPlayer1() const
{
    return m_player1;
}

void Game::setPlayer1(Player *player1)
{
    m_player1 = player1;
}

Board *Game::getBoard()
{
    return m_board;
}

Field* Game::getBoard(int i, int j)
{
    return (*m_board->getPboard())[i][j];
}

void Game::setBoard(Board *board)
{
    m_board = board;
}

void Game::setBoard(Field* f, int i, int j)
{
    (*m_board->getPboard())[i][j] = f;
}

GameScene *Game::getGameScene(){
    return m_gameScene;
}

void Game::setGameScene(GameScene *gameScene){
    m_gameScene = gameScene;
}

Turn Game::getTurn(){
    return m_turn;
}

void Game::setTurn(Turn turn){
    m_turn = turn;
}

int Game::getDifficulty() const
{
    return m_difficulty;
}

void Game::setDifficulty(int difficulty)
{
    m_difficulty = difficulty;
}

QLabel *Game::getNotification() const
{
    return m_notification;
}

void Game::setNotification(QLabel *notification)
{
    m_notification = notification;
}

void Game::setStatuses(QLabel *playerStatuses){
    m_statuses = playerStatuses;
}

bool Game::checkNoMoreMoves(PlayerType playerType, Board *board)
{
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10; j++){
            Figurine *f = (*board->getPboard())[i][j]->getFigurine();
            if(f && f->getOwner() == playerType){
                if(f->getFigurineType() != FigurineType::Bomb && f->getFigurineType() != FigurineType::Flag){
                    return false;
                }
            }
        }
    }
    return true;
}

bool Game::getGameEnded() const
{
    return m_gameEnded;
}

void Game::checkEvaluation(int evaluationScore){
    if(evaluationScore >= 9996 || evaluationScore <= -9996){
        m_gameEnded = true;
    }
}
