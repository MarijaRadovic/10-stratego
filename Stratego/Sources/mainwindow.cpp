#include "../Headers/mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>

int getRandomNumber(int a, int b){
    return ((rand() % (b-a+1)) + a);
}

std::vector<pState> presets;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    MainWindow::setWindowTitle("Stratego");
    ui->stackedWidget->setCurrentIndex(0);
    backIndex = 0;
    setWindowFlags(Qt::Window | Qt::CustomizeWindowHint | Qt::WindowTitleHint |
                    Qt::WindowSystemMenuHint | Qt::WindowMinimizeButtonHint | Qt::WindowCloseButtonHint );

    //setWindowFlags(Qt::tool);
    show();
    selectedDifficulty = GameDifficulty::MEDIUM;
    initializeMusic();
    initializeNotification();
    initializeSetupLayout();
    initializeMainLayout();
    connectSignalsAndSlots();
}

MainWindow::~MainWindow()
{
    delete ui;
    //delete player;
    //delete game;
//    delete easy_game_song;
//    delete medium_game_song;
//    delete hard_game_song;
//    delete buttons_click2;
//    delete on_off_click;
//    delete radio_button_click;
//    delete playlistsound;
//    delete playlistmedium;
//    delete playlisteasy;
//    delete playlisthard;
    delete setupLayout;
    delete setupViewTable;
    delete setupViewFigurines;
    //delete setupSceneTable;
    //delete setupSceneFigurine;
    delete pbSetupAutoComplete;
    delete pbSetupDone;
    delete pbSetupBack;
    delete mainLayout;
    delete mainView;

    delete pbMainOptions;
    delete pbMainQuit;

    delete gameScene;
    //delete playingBoard;

    delete notification;
    delete playerStatuses;

}

void MainWindow::back_clicked(){
    ui->stackedWidget->setCurrentIndex(backIndex);
    if(backIndex == 0)
        this->showNormal();
    if(backIndex == 5)
        this->showFullScreen();
    if(backIndex == 2 || backIndex == 5){
        //player->stop();

        musicOnOff(true);

    } else {
        //player->play();
    }
}

void MainWindow::newgame_clicked(){
    backIndex = ui->stackedWidget->currentIndex();
    ui->stackedWidget->setCurrentIndex(2);
    //player->stop();

    loadPresets();

    initializeSetupScenes();

    initializeNewGame();

    initializeGameScene();

    //notification->setText("Game is begining! \n Make the first move!");


    this->showFullScreen();

    setProbability();
    musicOnOff(true);
}

void MainWindow::continuegame_clicked(){
    if(m_gameStarted){
        int difficulty = 0;
        switch (selectedDifficulty) {
        case GameDifficulty::EASY:
            difficulty = 20;
            break;
        case GameDifficulty::MEDIUM:
            difficulty = 50;
            break;
        case GameDifficulty::HARD:
            difficulty = 80;
            break;
        }
        if(difficulty != game->getDifficulty())
            return;
        backIndex = ui->stackedWidget->currentIndex();
        ui->stackedWidget->setCurrentIndex(5);
        //player->play();
        this->showFullScreen();
        //player->stop();
        musicOnOff(true);
    }
}

void MainWindow::options_clicked(){
    backIndex = ui->stackedWidget->currentIndex();
    ui->stackedWidget->setCurrentIndex(3);
    //player->play();

    musicOnOff(false);
}

void MainWindow::exit_clicked(){
    close();

    exit(0);
}


void MainWindow::options_rules_clicked(){
    ui->stackedWidget->setCurrentIndex(4);
    //player->play();
    initializeListRules();

    musicOnOff(false);
}

void MainWindow::rules_back_clicked(){
    ui->stackedWidget->setCurrentIndex(3);
    //player->play();

    musicOnOff(false);
}

void MainWindow::setup_done_clicked(){
    if(playingBoard->checkPlayerSetup()){

        ui->stackedWidget->setCurrentIndex(5);

        musicOnOff(true);

        game->setBoard(playingBoard);
        game->setGameScene(gameScene);
        gameScene->setGame(game);
        gameScene->setBoard(playingBoard);
        game->drawTable(playingBoard,true);
        m_gameStarted = true;
    }
    else{
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        if(!playingBoard->getFirstRowFree())
            msgBox.setText("Figurines must not be placed in the highest row!");
        else if(!playingBoard->getAllFigurinesSet())
            msgBox.setText("All figurines must be placed in 4 rows!");
        else if(!playingBoard->getBombsNotBlockingWay())
            msgBox.setText("Bombs must not block the moving area!");

        msgBox.exec();
    }

}

void MainWindow::setup_back_clicked(){
    ui->stackedWidget->setCurrentIndex(backIndex);
    backIndex = 0;
    //player->play();
    this->showNormal();

    musicOnOff(false);
}

void MainWindow::main_options_clicked(){
    backIndex = ui->stackedWidget->currentIndex();
    ui->stackedWidget->setCurrentIndex(3);
    this->showNormal();
    //player->play();

    musicOnOff(false);
}

void MainWindow::main_quit_clicked(){
    ui->stackedWidget->setCurrentIndex(0);
    //player->play();
    this->showNormal();

    musicOnOff(false);
}

int MainWindow::volume() const
{
    return ui->horizontalSlider->value();
}

void MainWindow::setVolume(int volume)
{
    Q_UNUSED(volume)
    //player->setVolume(volume);
//    if(selectedDifficulty == GameDifficulty::EASY)
//        easy_game_song->setVolume(volume);
//    else if(selectedDifficulty == GameDifficulty::MEDIUM)
//        medium_game_song->setVolume(volume);
//    else
//        hard_game_song->setVolume(volume);
}

void MainWindow::on_pbOnOff_toggled(bool checked)
{
    if(checked){
        ui->pbOnOff->setIcon(QIcon(":/icons/Icons/off.jpg"));
        volumeValue = ui->horizontalSlider->value();
        //player->setMuted(true);


        musicMuteUnmute(true);

        ui->horizontalSlider->setValue(0);
        //on_off_click->play();
    } else {
        ui->pbOnOff->setIcon(QIcon(":/icons/Icons/on.jpg"));
        //on_off_click->play();
        //player->setMuted(false);

        musicMuteUnmute(false);

        ui->horizontalSlider->setValue(volumeValue);
    }
}

void MainWindow::on_pbExit_pressed()
{
    //buttons_click2->setVolume(10);
    //buttons_click2->play();
}

void MainWindow::on_pbOptions_pressed()
{
    //buttons_click2->setVolume(10);
    //buttons_click2->play();
}

void MainWindow::on_pbContinue_pressed()
{
    //buttons_click2->setVolume(10);
    //buttons_click2->play();
}

void MainWindow::on_pbNewGame_pressed()
{
    //buttons_click2->setVolume(10);
    //buttons_click2->play();
}


void MainWindow::on_pbMPback_pressed()
{
    //buttons_click2->setVolume(10);
    //buttons_click2->play();
}

void MainWindow::on_pbOptionsRules_pressed()
{
    //buttons_click2->setVolume(10);
    //buttons_click2->play();
}

void MainWindow::on_pbOptionsBack_pressed()
{
    //buttons_click2->setVolume(10);
   // buttons_click2->play();
}

void MainWindow::on_pbRulesBack_pressed()
{
    //buttons_click2->setVolume(10);
    //buttons_click2->play();
}

void MainWindow::on_pbSetupAutoCompletePressed(){
    srand(time(0));
    pState board = playingBoard->getPboard();
    std::vector<Figurine*> figs;
    QList<QGraphicsItem*> listItems = setupSceneFigurine->items();
    for(QGraphicsItem* item : listItems){
        Figurine *fig =  qgraphicsitem_cast<Figurine *>(item);
        figs.push_back(fig);
        setupSceneFigurine->removeItem(fig);
        fig->setSelected(false);
    }
    bool shouldBreak;
    for(Figurine *fig : figs){
        int k = getRandomNumber(6, 9);
        int l = getRandomNumber(0, 9);
        if(!(*board)[k][l]->getFigurine()){
            (*board)[k][l]->setFigurine(fig);
            fig->setXPos(l*120);
            fig->setYPos((k-5)*120);
            fig->setSize(120);
            setupSceneTable->addItem(fig);
        }
        else{
            shouldBreak = false;
            for(int i = 6; i < 10; i++){
                for(int j = 0; j < 10; j++){
                    if(!(*board)[i][j]->getFigurine()){
                        (*board)[i][j]->setFigurine(fig);
                        fig->setXPos(j*120);
                        fig->setYPos((i-5)*120);
                        fig->setSize(120);
                        setupSceneTable->addItem(fig);
                        shouldBreak = true;
                        break;
                    }
                }
                if(shouldBreak)
                    break;
            }
        }
    }
    setupSceneFigurine->status = SelectingStatus::SelectedMoved;
    setupSceneTable->update(setupSceneTable->getView()->rect());
}

void MainWindow::on_pbSetupDonePressed(){
    //buttons_click2->setVolume(10);
    //buttons_click2->play();
}

void MainWindow::on_pbSetupBackPressed(){
    //buttons_click2->setVolume(10);
    //buttons_click2->play();
}

void MainWindow::on_pbMainOptionsPressed(){
    //buttons_click2->setVolume(10);
    //buttons_click2->play();
}

void MainWindow::on_pbMainQuitPressed(){
    //buttons_click2->setVolume(10);
    //buttons_click2->play();
}

void MainWindow::on_rbEasy_pressed()
{
    selectedDifficulty = GameDifficulty::EASY;
    //radio_button_click->setVolume(10);
    //radio_button_click->play();
}

void MainWindow::on_rbMedium_pressed()
{
    selectedDifficulty = GameDifficulty::MEDIUM;
    //radio_button_click->setVolume(10);
   // radio_button_click->play();
}

void MainWindow::on_rbHard_pressed()
{
    selectedDifficulty = GameDifficulty::HARD;
    //radio_button_click->setVolume(10);
    //radio_button_click->play();
}

void MainWindow::initializeListRules()
{
    QListWidgetItem *setup = new QListWidgetItem;
    QListWidgetItem *object = new QListWidgetItem;
    QListWidgetItem *game = new QListWidgetItem;
    QListWidgetItem *moving = new QListWidgetItem;
    QListWidgetItem *attack = new QListWidgetItem;
    QListWidgetItem *strategy = new QListWidgetItem;
    QListWidgetItem *rank = new QListWidgetItem;
    QListWidgetItem *end = new QListWidgetItem;

    QFont f;
    f = setup->font();
    f.setPointSize(30);

    setup->setFont(f);
    setup->setTextColor(QColor(234, 211, 156));
    setup->setText("Setup");
    ui->listWidget->setStyleSheet("color:rgb(255, 255, 243);");

    ui->listWidget->addItem(setup);
    ui->listWidget->addItem("1. Set up your armies using the strategy hints and rules for \n"
                            "   movement and attacking that are discussed below.");
    ui->listWidget->addItem("2. Only one piece can occupy a square. Place them anywhere in \n"
                            "   the last four rows on your half of the gameboard. The two middle\n"
                            "   rows are left unoccupied at the start of the game.\n");
    object->setFont(f);
    object->setTextColor(QColor(234, 211, 156));
    object->setText("Object of the Game");
    ui->listWidget->addItem(object);
    ui->listWidget->addItem("1. The object of the game is to capture your opponent's flag.\n");

    game->setFont(f);
    game->setTextColor(QColor(234, 211, 156));
    game->setText("Game play");
    ui->listWidget->addItem(game);
    ui->listWidget->addItem("1. You and your opponent alternate turns.");
    ui->listWidget->addItem("2. On your turn you can do one of the following:");
    ui->listWidget->addItem("       Move - one of your playing pieces to an open adjacent space");
    ui->listWidget->addItem("       Attack - one of your opponent's playing pieces\n");

    moving->setFont(f);
    moving->setTextColor(QColor(234, 211, 156));
    moving->setText("Moving pieces");
    ui->listWidget->addItem(moving);
    ui->listWidget->addItem("1. Pieces move one square at a time, forward, backward or\n"
                            "   sideways. (Exception: see Special Scout Privilege, Rule 6).");
    ui->listWidget->addItem("2. Pieces cannot move diagonally. They cannot jump over another\n"
                            "   piece. They cannot move onto a square already occupied by\n"
                            "   another piece (unless attacking).");
    ui->listWidget->addItem("3. Pieces cannot jump over or move onto the two areas in the\n"
                            "   center of the gameboard.");
    ui->listWidget->addItem("4. A piece cannot move back and forth between the same two\n"
                            "   squares in three consecutive turns.");
    ui->listWidget->addItem("5. Only one piece can be moved on a turn.");
    ui->listWidget->addItem("6. Special Scout Privilege: A Scout can move any number of open\n"
                            "   squares forward, backward, or sideways. But remember, this\n"
                            "   movement will let your opponent know the value of that piece.\n");

    attack->setFont(f);
    attack->setTextColor(QColor(234, 211, 156));
    attack->setText("Attacking pieces");
    ui->listWidget->addItem(attack);
    ui->listWidget->addItem("1. Attack Position: When both players occupy adjacent spaces\n"
                            "   either back to back, side to side, or face to face, they are in a\n"
                            "   position to attack.");
    ui->listWidget->addItem("2. How To Attack: To attack on your turn, click on your figurine,\n"
                            "   then (if it is allowed) click on the enemy.");
    ui->listWidget->addItem("3. The piece with the lower rank (and lower number) is captured\n"
                            "   and removed from the board. If your piece (the attacking piece)\n"
                            "   is the remaining and winning piece, it moves into the space\n"
                            "   formerly occupied by the defending piece.\n"
                            "   If the remaining and winning piece is the defending piece, it\n"
                            "   stays on the square it was in when it was attacked.");
    ui->listWidget->addItem("4. When pieces of the same rank battle, both pieces are removed\n"
                            "   from the game.");
    ui->listWidget->addItem("5. Attacking is always optional.\n");

    rank->setFont(f);
    rank->setTextColor(QColor(234, 211, 156));
    rank->setText("Rank");
    ui->listWidget->addItem(rank);
    ui->listWidget->addItem("1. A Marshal (Number 10) outranks a General (Number 9) and\n"
                            "   any other lower-ranking piece. A General (Number 9) outranks a\n"
                            "   Colonel (Number 8) and any lower-ranking (but higher numbered)\n"
                            "   piece. A Colonel (Number 8) outranks a Major (Number 7) and\n"
                            "   so on down to the Spy which is the lowest-ranking piece.");
    ui->listWidget->addItem("2. Special Miner Privilege - When any piece (except a Miner -\n"
                            "   ranked 3) strikes a Bomb, both pieces are lost and removed from\n"
                            "   the board. When a Miner strikes a Bomb, the Bomb is defused\n"
                            "   and removed from the gameboard. The Miner then moves into\n"
                            "   the Bomb's space on the board. Bombs cannot attack or move.");
    ui->listWidget->addItem("3. Special Spy Privilege: A Spy has no numeral rank. If any piece\n"
                            "   attacks it, it is captured and removed from the board. But the\n"
                            "   Spy has a unique attacking privilege.\n"
                            "   It is the only piece that can outrank a Marshal providing the\n"
                            "   Spy attacks the Marshal first. If the Marshal attacks first\n"
                            "   then the Spy is removed.\n");


    strategy->setFont(f);
    strategy->setTextColor(QColor(234, 211, 156));
    strategy->setText("Strategy Hints");
    ui->listWidget->addItem(strategy);
    ui->listWidget->addItem("1. Place Bombs around the Flag to protect it. But place a Bomb or\n"
                            "   two elsewhere to confuse your opponent.");
    ui->listWidget->addItem("2. Put a few high-ranking pieces in the front line, but be careful!\n"
                            "   If you lose them early in the game you're in a weak position.");
    ui->listWidget->addItem("3. Scouts should be in the front lines to help you discover the\n"
                            "   strength of opposing pieces.");
    ui->listWidget->addItem("4. Place some Miners in the rear for the end of the game, where\n"
                            "   they will be needed to defuse Bombs.\n");

    end->setFont(f);
    end->setTextColor(QColor(234, 211, 156));
    end->setText("End of the game");
    ui->listWidget->addItem(end);
    ui->listWidget->addItem("1. The first player to attack an opponent's Flag captures it and wins\n"
                            "   the game.");
    ui->listWidget->addItem("2. If all of your moveable pieces have been removed and you\n"
                            "   cannot move or attack on a turn, you lose the game");
}

void MainWindow::initializeSetupSceneButtons()
{
    pbSetupAutoComplete = initializeButton("ADD ALL");
    pbSetupDone = initializeButton("DONE");
    pbSetupBack = initializeButton("BACK");

    pbSetupAutoComplete->setFixedSize(150,50);
    pbSetupBack->setFixedSize(150, 50);
    pbSetupDone->setFixedSize(150, 50);

    pbSetupAutoComplete->setStyleSheet("QPushButton {"
                                       "color: rgb(234, 211, 156);"
                                       "font: 12pt \"Ubuntu\"; "
                                       "background-color: rgb(102, 34, 37);"
                                       "border: 2px solid rgb(234, 211, 156);"
                                       "border-radius: 15px;border-style: outset;padding: 5px;"
                                       "}"
                                       " QPushButton:hover {"
                                       "background-color: rgb(157, 51, 31);"
                                       "border: 3px solid rgb(188, 109, 79);"
                                       "}");

    pbSetupDone->setStyleSheet("QPushButton {"
                               "color: rgb(234, 211, 156);"
                               "font: 12pt \"Ubuntu\"; "
                               "background-color: rgb(102, 34, 37);"
                               "border: 2px solid rgb(234, 211, 156);"
                               "border-radius: 15px;border-style: outset;padding: 5px;"
                               "}"
                               " QPushButton:hover {"
                               "background-color: rgb(157, 51, 31);"
                               "border: 3px solid rgb(188, 109, 79);"
                               "}");

    pbSetupBack->setStyleSheet("QPushButton {"
                               "color: rgb(234, 211, 156);"
                               "font: 12pt \"Ubuntu\"; "
                               "background-color: rgb(102, 34, 37);"
                               "border: 2px solid rgb(234, 211, 156);"
                               "border-radius: 15px;"
                               "border-style: outset;padding: 5px;"
                               "} "
                               "QPushButton:hover {"
                               "background-color: rgb(157, 51, 31);"
                               "border: 3px solid rgb(188, 109, 79);"
                               "}");

    connect(pbSetupAutoComplete, &QPushButton::pressed, this, &MainWindow::on_pbSetupAutoCompletePressed);
    connect(pbSetupDone, &QPushButton::pressed, this, &MainWindow::on_pbSetupDonePressed);
    connect(pbSetupBack, &QPushButton::pressed, this, &MainWindow::on_pbSetupBackPressed);
}

void MainWindow::initializeNotification(){
    notification = new QLabel(ui->MainScreen);
    playerStatuses = new QLabel(ui->MainScreen);

    QFont f( "Arial", 20, QFont::Bold);
    notification->setFont(f);
    notification->setStyleSheet("QLabel{color:rgb(234, 211, 156);}");

    playerStatuses->setFont(f);
    playerStatuses->setStyleSheet("QLabel{color:rgb(215, 214, 255);}");

    notification->setText("Game is begining! \n Make the first move!");
    notification->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    notification->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    playerStatuses->setText("Battle status:\nYou: 40 figurines\nOpponent: 40 figurines");
    playerStatuses->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    playerStatuses->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
}

void MainWindow::initializeMainSceneButtons()
{
    pbMainOptions = initializeButton("OPTIONS");
    pbMainQuit = initializeButton("QUIT");

    connect(pbMainOptions, &QPushButton::pressed, this, &MainWindow::on_pbMainOptionsPressed);
    connect(pbMainQuit, &QPushButton::pressed, this, &MainWindow::on_pbMainQuitPressed);

    pbMainOptions->setStyleSheet("QPushButton "
                                 "{"
                                 "color: rgb(234, 211, 156);"
                                 "font: 12pt \"Ubuntu\"; "
                                 "background-color: rgb(102, 34, 37);"
                                 "border: 2px solid rgb(234, 211, 156);"
                                 "border-radius: 15px;border-style: outset;padding: 5px;"
                                 "} "
                                 "QPushButton:hover {background-color: rgb(157, 51, 31);"
                                 "border: 3px solid rgb(188, 109, 79);"
                                 "}");

    pbMainQuit->setStyleSheet("QPushButton "
                              "{"
                              "color: rgb(234, 211, 156);"
                              "font: 12pt \"Ubuntu\"; "
                              "background-color: rgb(102, 34, 37);"
                              "border: 2px solid rgb(234, 211, 156);"
                              "border-radius: 15px;border-style: outset;padding: 5px;"
                              "} "
                              "QPushButton:hover "
                              "{"
                              "background-color: rgb(157, 51, 31);"
                              "border: 3px solid rgb(188, 109, 79);"
                              "}");
}

void MainWindow::loadPresets()
{
    srand(time(0));
    presets.resize(6);
    if(presets[0])
        delete presets[0];
    if(presets[1])
        delete presets[1];
    if(presets[2])
        delete presets[2];
    if(presets[3])
        delete presets[3];
    if(presets[4])
        delete presets[4];
    if(presets[5])
        delete presets[5];
    presets[0] = findPreset(0);
    presets[1] = findPreset(1);
    presets[2] = findPreset(2);
    presets[3] = findPreset(3);
    presets[4] = findPreset(4);
    presets[5] = findPreset(5);
    m_computerPresetIndex = getRandomNumber(0,5);
}

void MainWindow::initializeSetupScenes()
{
    if(setupSceneTable)
        delete setupSceneTable;
    if(setupSceneFigurine)
        delete setupSceneFigurine;

    setupSceneTable = new TableSetupScene(ui->SetupScreen);
    setupSceneFigurine = new TableFigurineScene(ui->SetupScreen);
    setupSceneFigurine->setView(setupViewFigurines);
    setupSceneTable->setView(setupViewTable);

    /* Setting up setup scenes */
    int squareSize = 120;
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 5; j++){
            Field *field = new Field(FieldType::Free,squareSize*i,squareSize*j);
            if(i == 2 && j == 0)
                field->setSpecialFieldFlag(3);
            if(i == 3 && j == 0)
                field->setSpecialFieldFlag(4);
            if(i == 6 && j == 0)
                field->setSpecialFieldFlag(7);
            if(i == 7 && j == 0)
                field->setSpecialFieldFlag(8);

            setupSceneTable->addItem(field);
        }
    }

    setupSceneTable->setOtherScene(setupSceneFigurine);
    setupSceneFigurine->setOtherScene(setupSceneTable);

    Figurine *figurines[40];

    figurines[0] = new Figurine(PlayerType::Human, FigurineType::Flag);
    figurines[1] = new Figurine(PlayerType::Human, FigurineType::Marshal);
    figurines[2] = new Figurine(PlayerType::Human, FigurineType::General);
    figurines[3] = new Figurine(PlayerType::Human, FigurineType::Colonel);
    figurines[4] = new Figurine(PlayerType::Human, FigurineType::Colonel);
    figurines[5] = new Figurine(PlayerType::Human, FigurineType::Major);
    figurines[6] = new Figurine(PlayerType::Human, FigurineType::Major);
    figurines[7] = new Figurine(PlayerType::Human, FigurineType::Major);
    figurines[8] = new Figurine(PlayerType::Human, FigurineType::Captain);
    figurines[9] = new Figurine(PlayerType::Human, FigurineType::Captain);
    figurines[10] = new Figurine(PlayerType::Human, FigurineType::Captain);
    figurines[11] = new Figurine(PlayerType::Human, FigurineType::Captain);
    figurines[12] = new Figurine(PlayerType::Human, FigurineType::Lieutenant);
    figurines[13] = new Figurine(PlayerType::Human, FigurineType::Lieutenant);
    figurines[14] = new Figurine(PlayerType::Human, FigurineType::Lieutenant);
    figurines[15] = new Figurine(PlayerType::Human, FigurineType::Lieutenant);
    figurines[16] = new Figurine(PlayerType::Human, FigurineType::Sergeant);
    figurines[17] = new Figurine(PlayerType::Human, FigurineType::Sergeant);
    figurines[18] = new Figurine(PlayerType::Human, FigurineType::Sergeant);
    figurines[19] = new Figurine(PlayerType::Human, FigurineType::Sergeant);
    figurines[20] = new Figurine(PlayerType::Human, FigurineType::Miner);
    figurines[21] = new Figurine(PlayerType::Human, FigurineType::Miner);
    figurines[22] = new Figurine(PlayerType::Human, FigurineType::Miner);
    figurines[23] = new Figurine(PlayerType::Human, FigurineType::Miner);
    figurines[24] = new Figurine(PlayerType::Human, FigurineType::Miner);
    figurines[25] = new Figurine(PlayerType::Human, FigurineType::Scout);
    figurines[26] = new Figurine(PlayerType::Human, FigurineType::Scout);
    figurines[27] = new Figurine(PlayerType::Human, FigurineType::Scout);
    figurines[28] = new Figurine(PlayerType::Human, FigurineType::Scout);
    figurines[29] = new Figurine(PlayerType::Human, FigurineType::Scout);
    figurines[30] = new Figurine(PlayerType::Human, FigurineType::Scout);
    figurines[31] = new Figurine(PlayerType::Human, FigurineType::Scout);
    figurines[32] = new Figurine(PlayerType::Human, FigurineType::Scout);
    figurines[33] = new Figurine(PlayerType::Human, FigurineType::Spy);
    figurines[34] = new Figurine(PlayerType::Human, FigurineType::Bomb);
    figurines[35] = new Figurine(PlayerType::Human, FigurineType::Bomb);
    figurines[36] = new Figurine(PlayerType::Human, FigurineType::Bomb);
    figurines[37] = new Figurine(PlayerType::Human, FigurineType::Bomb);
    figurines[38] = new Figurine(PlayerType::Human, FigurineType::Bomb);
    figurines[39] = new Figurine(PlayerType::Human, FigurineType::Bomb);

    int ind = 0;
    int sqSize = 70;
    for(int i = 0; i < 2; i++){
        for(int j = 0; j < 20; j++){
            figurines[ind]->setSize(sqSize);
            figurines[ind]->setYPos(i*sqSize);
            figurines[ind]->setXPos(j*sqSize);
            setupSceneFigurine->addItem(figurines[ind++]);
        }
    }

    setupViewTable->setScene(setupSceneTable);
    setupViewFigurines->setScene(setupSceneFigurine);
}

void MainWindow::initializeNewGame()
{
    game = new Game();
    Player *p1 = new Player(PlayerType::Human);
    Player *p2 = new Player(PlayerType::Computer);
    game->setPlayer1(p1);
    game->setPlayer2(p2);
    game->setNotification(notification);
    game->setStatuses(playerStatuses);
    playingBoard = new Board();
    setupSceneTable->setBoard(playingBoard);
    setupSceneFigurine->setBoard(playingBoard);
    p1->setPlayerTable(playingBoard);
    p2->setPlayerTable(playingBoard);
    for(int i = 0; i < 4; i++)
        for(int j = 0; j < 10; j++)
            playingBoard->setField(i, j, presets[m_computerPresetIndex]);
}

void MainWindow::initializeGameScene()
{
    if(gameScene)
        delete gameScene;
    gameScene = new GameScene(ui->MainScreen);
    gameScene->setView(mainView);

    int squareSize = 90;
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10; j++){
            Field *field = (*playingBoard->getPboard())[i][j];
            field->setXPos(90*j);
            field->setYPos(90*i);
            field->setWidth(squareSize);
            field->setHeight(squareSize);
            field->setAvailable(false);
            gameScene->addItem(field);
        }
    }

    mainView->setScene(gameScene);

}

void MainWindow::initializeSetupLayout()
{
    /*Setup Layout*/
    setupLayout = new QGridLayout();
    setupViewTable = new QGraphicsView(ui->SetupScreen);
    setupViewFigurines = new QGraphicsView(ui->SetupScreen);
    setupSceneTable = new TableSetupScene(ui->SetupScreen);
    setupSceneFigurine = new TableFigurineScene(ui->SetupScreen);
    setupSceneFigurine->setView(setupViewFigurines);
    setupSceneTable->setView(setupViewTable);

    setupSceneTable->setOtherScene(setupSceneFigurine);
    setupSceneFigurine->setOtherScene(setupSceneTable);

    /* Setting up setup scenes */
    int squareSize = 120;
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 5; j++){
            Field *field = new Field(FieldType::Free,squareSize*i,squareSize*j);
            if(i == 2 && j == 0)
                field->setSpecialFieldFlag(3);
            if(i == 3 && j == 0)
                field->setSpecialFieldFlag(4);
            if(i == 6 && j == 0)
                field->setSpecialFieldFlag(7);
            if(i == 7 && j == 0)
                field->setSpecialFieldFlag(8);

            setupSceneTable->addItem(field);
        }
    }
    setupViewTable->setScene(setupSceneTable);
    setupViewFigurines->setScene(setupSceneFigurine);
    setupViewTable->setStyleSheet("background-color: rgb(102, 34, 37);border: 2px solid rgb(234, 211, 156);border-radius: 15px");
    setupViewFigurines->setStyleSheet("background-color: rgb(102, 34, 37);border: 2px solid rgb(234, 211, 156);border-radius: 15px");

    Figurine *figurines[40];

    figurines[0] = new Figurine(PlayerType::Human, FigurineType::Flag);
    figurines[1] = new Figurine(PlayerType::Human, FigurineType::Marshal);
    figurines[2] = new Figurine(PlayerType::Human, FigurineType::General);
    figurines[3] = new Figurine(PlayerType::Human, FigurineType::Colonel);
    figurines[4] = new Figurine(PlayerType::Human, FigurineType::Colonel);
    figurines[5] = new Figurine(PlayerType::Human, FigurineType::Major);
    figurines[6] = new Figurine(PlayerType::Human, FigurineType::Major);
    figurines[7] = new Figurine(PlayerType::Human, FigurineType::Major);
    figurines[8] = new Figurine(PlayerType::Human, FigurineType::Captain);
    figurines[9] = new Figurine(PlayerType::Human, FigurineType::Captain);
    figurines[10] = new Figurine(PlayerType::Human, FigurineType::Captain);
    figurines[11] = new Figurine(PlayerType::Human, FigurineType::Captain);
    figurines[12] = new Figurine(PlayerType::Human, FigurineType::Lieutenant);
    figurines[13] = new Figurine(PlayerType::Human, FigurineType::Lieutenant);
    figurines[14] = new Figurine(PlayerType::Human, FigurineType::Lieutenant);
    figurines[15] = new Figurine(PlayerType::Human, FigurineType::Lieutenant);
    figurines[16] = new Figurine(PlayerType::Human, FigurineType::Sergeant);
    figurines[17] = new Figurine(PlayerType::Human, FigurineType::Sergeant);
    figurines[18] = new Figurine(PlayerType::Human, FigurineType::Sergeant);
    figurines[19] = new Figurine(PlayerType::Human, FigurineType::Sergeant);
    figurines[20] = new Figurine(PlayerType::Human, FigurineType::Miner);
    figurines[21] = new Figurine(PlayerType::Human, FigurineType::Miner);
    figurines[22] = new Figurine(PlayerType::Human, FigurineType::Miner);
    figurines[23] = new Figurine(PlayerType::Human, FigurineType::Miner);
    figurines[24] = new Figurine(PlayerType::Human, FigurineType::Miner);
    figurines[25] = new Figurine(PlayerType::Human, FigurineType::Scout);
    figurines[26] = new Figurine(PlayerType::Human, FigurineType::Scout);
    figurines[27] = new Figurine(PlayerType::Human, FigurineType::Scout);
    figurines[28] = new Figurine(PlayerType::Human, FigurineType::Scout);
    figurines[29] = new Figurine(PlayerType::Human, FigurineType::Scout);
    figurines[30] = new Figurine(PlayerType::Human, FigurineType::Scout);
    figurines[31] = new Figurine(PlayerType::Human, FigurineType::Scout);
    figurines[32] = new Figurine(PlayerType::Human, FigurineType::Scout);
    figurines[33] = new Figurine(PlayerType::Human, FigurineType::Spy);
    figurines[34] = new Figurine(PlayerType::Human, FigurineType::Bomb);
    figurines[35] = new Figurine(PlayerType::Human, FigurineType::Bomb);
    figurines[36] = new Figurine(PlayerType::Human, FigurineType::Bomb);
    figurines[37] = new Figurine(PlayerType::Human, FigurineType::Bomb);
    figurines[38] = new Figurine(PlayerType::Human, FigurineType::Bomb);
    figurines[39] = new Figurine(PlayerType::Human, FigurineType::Bomb);

    int ind = 0;
    int sqSize = 70;
    for(int i = 0; i < 2; i++){
        for(int j = 0; j < 20; j++){
            figurines[ind]->setSize(sqSize);
            figurines[ind]->setYPos(i*sqSize);
            figurines[ind]->setXPos(j*sqSize);
            setupSceneFigurine->addItem(figurines[ind++]);
        }
    }

    pbSetupAutoComplete = initializeButton("ADD ALL");
    pbSetupDone = initializeButton("DONE");
    pbSetupBack = initializeButton("BACK");

    pbSetupAutoComplete->setFixedSize(150,50);
    pbSetupBack->setFixedSize(150, 50);
    pbSetupDone->setFixedSize(150, 50);

    pbSetupAutoComplete->setStyleSheet("QPushButton {"
                                       "color: rgb(234, 211, 156);"
                                       "font: 12pt \"Ubuntu\"; "
                                       "background-color: rgb(102, 34, 37);"
                                       "border: 2px solid rgb(234, 211, 156);"
                                       "border-radius: 15px;border-style: outset;padding: 5px;"
                                       "}"
                                       " QPushButton:hover {"
                                       "background-color: rgb(157, 51, 31);"
                                       "border: 3px solid rgb(188, 109, 79);"
                                       "}");

    pbSetupDone->setStyleSheet("QPushButton {"
                               "color: rgb(234, 211, 156);"
                               "font: 12pt \"Ubuntu\"; "
                               "background-color: rgb(102, 34, 37);"
                               "border: 2px solid rgb(234, 211, 156);"
                               "border-radius: 15px;border-style: outset;padding: 5px;"
                               "}"
                               " QPushButton:hover {"
                               "background-color: rgb(157, 51, 31);"
                               "border: 3px solid rgb(188, 109, 79);"
                               "}");

    pbSetupBack->setStyleSheet("QPushButton {"
                               "color: rgb(234, 211, 156);"
                               "font: 12pt \"Ubuntu\"; "
                               "background-color: rgb(102, 34, 37);"
                               "border: 2px solid rgb(234, 211, 156);"
                               "border-radius: 15px;"
                               "border-style: outset;padding: 5px;"
                               "} "
                               "QPushButton:hover {"
                               "background-color: rgb(157, 51, 31);"
                               "border: 3px solid rgb(188, 109, 79);"
                               "}");

    connect(pbSetupAutoComplete, &QPushButton::pressed, this, &MainWindow::on_pbSetupAutoCompletePressed);
    connect(pbSetupDone, &QPushButton::pressed, this, &MainWindow::on_pbSetupDonePressed);
    connect(pbSetupBack, &QPushButton::pressed, this, &MainWindow::on_pbSetupBackPressed);

    setupLayout->addWidget(setupViewTable, 0, 0, 7, 10);
    setupLayout->addWidget(setupViewFigurines, 7, 0, 3, 9);
    setupLayout->addWidget(pbSetupAutoComplete, 8, 9, 1, 1);
    setupLayout->addWidget(pbSetupDone, 7, 9, 1, 1);
    setupLayout->addWidget(pbSetupBack, 9, 9, 1, 1);

    ui->SetupScreen->setLayout(setupLayout);
}

void MainWindow::initializeMainLayout()
{
    /*Main Layout*/
    /* Creating main layout, scene and view */
    mainLayout = new QGridLayout();

    mainView = new QGraphicsView(ui->MainScreen);
    gameScene = new GameScene(ui->MainScreen);
    gameScene->setView(mainView);
    mainView->setStyleSheet("border-image: url(:/Images/texture.jpg) 0 0 0 0 stretch stretch; border: 2px solid rgb(234, 211, 156);border-radius: 15px;");
    //border-image: url(:/Images/mainbackground.jpg) 0 0 0 0 stretch stretch;


    /* Setting up main scene */

    pbMainOptions = initializeButton("OPTIONS");
    pbMainQuit = initializeButton("QUIT");

    connect(pbMainOptions, &QPushButton::pressed, this, &MainWindow::on_pbMainOptionsPressed);
    connect(pbMainQuit, &QPushButton::pressed, this, &MainWindow::on_pbMainQuitPressed);

    pbMainOptions->setStyleSheet("QPushButton "
                                 "{"
                                 "color: rgb(234, 211, 156);"
                                 "font: 12pt \"Ubuntu\"; "
                                 "background-color: rgb(102, 34, 37);"
                                 "border: 2px solid rgb(234, 211, 156);"
                                 "border-radius: 15px;border-style: outset;padding: 5px;"
                                 "} "
                                 "QPushButton:hover {background-color: rgb(157, 51, 31);"
                                 "border: 3px solid rgb(188, 109, 79);"
                                 "}");

    pbMainQuit->setStyleSheet("QPushButton "
                              "{"
                              "color: rgb(234, 211, 156);"
                              "font: 12pt \"Ubuntu\"; "
                              "background-color: rgb(102, 34, 37);"
                              "border: 2px solid rgb(234, 211, 156);"
                              "border-radius: 15px;border-style: outset;padding: 5px;"
                              "} "
                              "QPushButton:hover "
                              "{"
                              "background-color: rgb(157, 51, 31);"
                              "border: 3px solid rgb(188, 109, 79);"
                              "}");

    mainView->setScene(gameScene);

    mainLayout->addWidget(mainView, 0, 0, 10, 8);
    mainLayout->addWidget(notification, 0, 8, 2, 2);
    mainLayout->addWidget(playerStatuses, 3, 8, 2, 2);
    mainLayout->addWidget(pbMainOptions, 8, 8, 1, 2);
    mainLayout->addWidget(pbMainQuit, 9, 8, 1, 2);

    ui->MainScreen->setLayout(mainLayout);
}

void MainWindow::connectSignalsAndSlots()
{
    /* Connecting signals and slots */

    //connect(game,&Game::endGame,this,&MainWindow::onEndGame);

    connect(ui->pbNewGame, &QPushButton::clicked, this, &MainWindow::newgame_clicked);
    connect(ui->pbContinue, &QPushButton::clicked, this, &MainWindow::continuegame_clicked);
    connect(ui->pbOptions, &QPushButton::clicked, this, &MainWindow::options_clicked);
    connect(ui->pbExit, &QPushButton::clicked, this, &MainWindow::exit_clicked);

    connect(ui->pbMPback, &QPushButton::clicked, this, &MainWindow::back_clicked);
    connect(ui->pbOptionsRules, &QPushButton::clicked, this, &MainWindow::options_rules_clicked);
    connect(ui->pbOptionsBack, &QPushButton::clicked, this, &MainWindow::back_clicked);
    connect(ui->pbRulesBack, &QPushButton::clicked, this, &MainWindow::rules_back_clicked);
    connect(pbSetupDone, &QPushButton::clicked, this, &MainWindow::setup_done_clicked);
    connect(pbSetupBack, &QPushButton::clicked, this, &MainWindow::setup_back_clicked);
    connect(pbMainOptions, &QPushButton::clicked, this, &MainWindow::main_options_clicked);
    connect(pbMainQuit, &QPushButton::clicked, this, &MainWindow::main_quit_clicked);


}

QPushButton *MainWindow::initializeButton(QString text)
{
    QPushButton *button = new QPushButton(text);
    button->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    return button;
}

void MainWindow::musicOnOff(bool on)
{
    Q_UNUSED(on)
//    if(on){
//        if(selectedDifficulty == GameDifficulty::EASY)
//            easy_game_song->play();
//        else if(selectedDifficulty == GameDifficulty::MEDIUM)
//            medium_game_song->play();
//        else
//            hard_game_song->play();
//    }
//    else{
//        if(selectedDifficulty == GameDifficulty::EASY)
//            easy_game_song->stop();
//        else if(selectedDifficulty == GameDifficulty::MEDIUM)
//            medium_game_song->stop();
//        else
//            hard_game_song->stop();
//    }
}

void MainWindow::musicMuteUnmute(bool shouldPlay)
{
    Q_UNUSED(shouldPlay)
//    if(selectedDifficulty == GameDifficulty::EASY)
//        easy_game_song->setMuted(shouldPlay);
//    else if(selectedDifficulty == GameDifficulty::MEDIUM)
//        medium_game_song->setMuted(shouldPlay);
//    else
//        hard_game_song->setMuted(shouldPlay);
}



void MainWindow::setProbability()
{
    switch(selectedDifficulty){
    case GameDifficulty::EASY:
        game->setDifficulty(20);
        break;
    case GameDifficulty::MEDIUM:
        game->setDifficulty(50);
        break;
    case GameDifficulty::HARD:
        game->setDifficulty(80);
        break;
    };
}


std::vector<std::vector<Field *>> *MainWindow::findPreset(int index)
{
    std::set<Figurine*> *figurines = new std::set<Figurine *>;

    initializeSetOfFigurines(figurines);

    std::vector<std::vector<Field *>> *currentPreset = new std::vector<std::vector<Field *>>;
    currentPreset->resize(4);
    for(int i = 0; i<4;i++){
      (*currentPreset)[i].resize(10);
    }

    if(index == 0){
        putFigurineInPreset(0,0,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(0,1,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(0,2,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(0,3,figurines,currentPreset,FigurineType::Flag);
        putFigurineInPreset(0,4,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(0,5,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(0,6,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(0,7,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(0,8,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(0,9,figurines,currentPreset,FigurineType::Scout);

        putFigurineInPreset(1,0,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(1,1,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(1,2,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(1,3,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(1,4,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(1,5,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(1,6,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(1,7,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(1,8,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(1,9,figurines,currentPreset,FigurineType::Sergeant);

        putFigurineInPreset(2,0,figurines,currentPreset,FigurineType::Major);
        putFigurineInPreset(2,1,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(2,2,figurines,currentPreset,FigurineType::Colonel);
        putFigurineInPreset(2,3,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(2,4,figurines,currentPreset,FigurineType::Major);
        putFigurineInPreset(2,5,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(2,6,figurines,currentPreset,FigurineType::Major);
        putFigurineInPreset(2,7,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(2,8,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(2,9,figurines,currentPreset,FigurineType::Colonel);

        putFigurineInPreset(3,0,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,1,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(3,2,figurines,currentPreset,FigurineType::General);
        putFigurineInPreset(3,3,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,4,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(3,5,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,6,figurines,currentPreset,FigurineType::Marshal);
        putFigurineInPreset(3,7,figurines,currentPreset,FigurineType::Spy);
        putFigurineInPreset(3,8,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(3,9,figurines,currentPreset,FigurineType::Scout);

    }
    else if(index == 1){

        putFigurineInPreset(0,0,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(0,1,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(0,2,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(0,3,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(0,4,figurines,currentPreset,FigurineType::Flag);
        putFigurineInPreset(0,5,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(0,6,figurines,currentPreset,FigurineType::Colonel);
        putFigurineInPreset(0,7,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(0,8,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(0,9,figurines,currentPreset,FigurineType::Captain);

        putFigurineInPreset(1,0,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(1,1,figurines,currentPreset,FigurineType::Major);
        putFigurineInPreset(1,2,figurines,currentPreset,FigurineType::Colonel);
        putFigurineInPreset(1,3,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(1,4,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(1,5,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(1,6,figurines,currentPreset,FigurineType::Spy);
        putFigurineInPreset(1,7,figurines,currentPreset,FigurineType::General);
        putFigurineInPreset(1,8,figurines,currentPreset,FigurineType::Major);
        putFigurineInPreset(1,9,figurines,currentPreset,FigurineType::Captain);

        putFigurineInPreset(2,0,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(2,1,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(2,2,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(2,3,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(2,4,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(2,5,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(2,6,figurines,currentPreset,FigurineType::Major);
        putFigurineInPreset(2,7,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(2,8,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(2,9,figurines,currentPreset,FigurineType::Miner);

        putFigurineInPreset(3,0,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,1,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,2,figurines,currentPreset,FigurineType::Marshal);
        putFigurineInPreset(3,3,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,4,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,5,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,6,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,7,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,8,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,9,figurines,currentPreset,FigurineType::Bomb);



    }
    else if(index == 2){
        putFigurineInPreset(3,0,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,1,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,2,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(3,3,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(3,4,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,5,figurines,currentPreset,FigurineType::General);
        putFigurineInPreset(3,6,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(3,7,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(3,8,figurines,currentPreset,FigurineType::Colonel);
        putFigurineInPreset(3,9,figurines,currentPreset,FigurineType::Scout);

        putFigurineInPreset(2,0,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(2,1,figurines,currentPreset,FigurineType::Marshal);
        putFigurineInPreset(2,2,figurines,currentPreset,FigurineType::Spy);
        putFigurineInPreset(2,3,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(2,4,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(2,5,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(2,6,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(2,7,figurines,currentPreset,FigurineType::Major);
        putFigurineInPreset(2,8,figurines,currentPreset,FigurineType::Colonel);
        putFigurineInPreset(2,9,figurines,currentPreset,FigurineType::Scout);

        putFigurineInPreset(1,0,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(1,1,figurines,currentPreset,FigurineType::Major);
        putFigurineInPreset(1,2,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(1,3,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(1,4,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(1,5,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(1,6,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(1,7,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(1,8,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(1,9,figurines,currentPreset,FigurineType::Major);

        putFigurineInPreset(0,0,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(0,1,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(0,2,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(0,3,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(0,4,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(0,5,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(0,6,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(0,7,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(0,8,figurines,currentPreset,FigurineType::Flag);
        putFigurineInPreset(0,9,figurines,currentPreset,FigurineType::Bomb);
    }
    else if(index == 3){
        putFigurineInPreset(3,0,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,1,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,2,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(3,3,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(3,4,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(3,5,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(3,6,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,7,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,8,figurines,currentPreset,FigurineType::Colonel);
        putFigurineInPreset(3,9,figurines,currentPreset,FigurineType::Scout);

        putFigurineInPreset(2,0,figurines,currentPreset,FigurineType::Major);
        putFigurineInPreset(2,1,figurines,currentPreset,FigurineType::Colonel);
        putFigurineInPreset(2,2,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(2,3,figurines,currentPreset,FigurineType::Major);
        putFigurineInPreset(2,4,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(2,5,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(2,6,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(2,7,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(2,8,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(2,9,figurines,currentPreset,FigurineType::General);

        putFigurineInPreset(1,0,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(1,1,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(1,2,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(1,3,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(1,4,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(1,5,figurines,currentPreset,FigurineType::Marshal);
        putFigurineInPreset(1,6,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(1,7,figurines,currentPreset,FigurineType::Flag);
        putFigurineInPreset(1,8,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(1,9,figurines,currentPreset,FigurineType::Spy);

        putFigurineInPreset(0,0,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(0,1,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(0,2,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(0,3,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(0,4,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(0,5,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(0,6,figurines,currentPreset,FigurineType::Major);
        putFigurineInPreset(0,7,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(0,8,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(0,9,figurines,currentPreset,FigurineType::Scout);
    }
    else if(index == 4){
        putFigurineInPreset(3,0,figurines,currentPreset,FigurineType::Marshal);
        putFigurineInPreset(3,1,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,2,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(3,3,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(3,4,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,5,figurines,currentPreset,FigurineType::General);
        putFigurineInPreset(3,6,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(3,7,figurines,currentPreset,FigurineType::Flag);
        putFigurineInPreset(3,8,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(3,9,figurines,currentPreset,FigurineType::Colonel);

        putFigurineInPreset(2,0,figurines,currentPreset,FigurineType::Colonel);
        putFigurineInPreset(2,1,figurines,currentPreset,FigurineType::Major);
        putFigurineInPreset(2,2,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(2,3,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(2,4,figurines,currentPreset,FigurineType::Major);
        putFigurineInPreset(2,5,figurines,currentPreset,FigurineType::Spy);
        putFigurineInPreset(2,6,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(2,7,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(2,8,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(2,9,figurines,currentPreset,FigurineType::Major);

        putFigurineInPreset(1,0,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(1,1,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(1,2,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(1,3,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(1,4,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(1,5,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(1,6,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(1,7,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(1,8,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(1,9,figurines,currentPreset,FigurineType::Scout);

        putFigurineInPreset(0,0,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(0,1,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(0,2,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(0,3,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(0,4,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(0,5,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(0,6,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(0,7,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(0,8,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(0,9,figurines,currentPreset,FigurineType::Sergeant);
    }
    else if(index == 5){
        putFigurineInPreset(3,0,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,1,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(3,2,figurines,currentPreset,FigurineType::Major);
        putFigurineInPreset(3,3,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(3,4,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(3,5,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(3,6,figurines,currentPreset,FigurineType::Spy);
        putFigurineInPreset(3,7,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(3,8,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(3,9,figurines,currentPreset,FigurineType::Scout);

        putFigurineInPreset(2,0,figurines,currentPreset,FigurineType::Colonel);
        putFigurineInPreset(2,1,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(2,2,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(2,3,figurines,currentPreset,FigurineType::Marshal);
        putFigurineInPreset(2,4,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(2,5,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(2,6,figurines,currentPreset,FigurineType::General);
        putFigurineInPreset(2,7,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(2,8,figurines,currentPreset,FigurineType::Captain);
        putFigurineInPreset(2,9,figurines,currentPreset,FigurineType::Colonel);

        putFigurineInPreset(1,0,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(1,1,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(1,2,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(1,3,figurines,currentPreset,FigurineType::Major);
        putFigurineInPreset(1,4,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(1,5,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(1,6,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(1,7,figurines,currentPreset,FigurineType::Major);
        putFigurineInPreset(1,8,figurines,currentPreset,FigurineType::Scout);
        putFigurineInPreset(1,9,figurines,currentPreset,FigurineType::Scout);

        putFigurineInPreset(0,0,figurines,currentPreset,FigurineType::Flag);
        putFigurineInPreset(0,1,figurines,currentPreset,FigurineType::Bomb);
        putFigurineInPreset(0,2,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(0,3,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(0,4,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(0,5,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(0,6,figurines,currentPreset,FigurineType::Lieutenant);
        putFigurineInPreset(0,7,figurines,currentPreset,FigurineType::Sergeant);
        putFigurineInPreset(0,8,figurines,currentPreset,FigurineType::Miner);
        putFigurineInPreset(0,9,figurines,currentPreset,FigurineType::Sergeant);
    }

    return currentPreset;
}

void MainWindow::putFigurineInPreset(int i, int j, std::set<Figurine *> *figurines,
                                     std::vector<std::vector<Field *> > *currentPreset,
                                     FigurineType ft)
{
    bool found = false;
    Field *field;
    int squareSize = 90;
    field = new Field
            (FieldType::Occupied,i*squareSize,j*squareSize);

    for(auto it = figurines->begin(); it!=figurines->end(); it++){
        if((*it)->getFigurineType() == ft){
            Figurine *fig = *it;
            fig->setXPos(squareSize*i);
            fig->setYPos(j*squareSize);
            field->setFigurine(fig);

            (*currentPreset)[i][j] = field;
            figurines->erase(it);
            found = true;
            break;
        }
    }
    if(!found){
        std::cerr<<"Figurine can't be placed!"<<std::endl;
        throw "Figurine can't be placed!";
    }
}

void MainWindow::initializeSetOfFigurines(std::set<Figurine *> *figurines)
{
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Flag));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Marshal));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::General));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Colonel));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Colonel));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Major));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Major));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Major));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Captain));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Captain));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Captain));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Captain));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Lieutenant));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Lieutenant));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Lieutenant));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Lieutenant));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Sergeant));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Sergeant));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Sergeant));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Sergeant));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Miner));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Miner));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Miner));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Miner));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Miner));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Scout));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Scout));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Scout));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Scout));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Scout));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Scout));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Scout));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Scout));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Spy));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Bomb));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Bomb));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Bomb));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Bomb));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Bomb));
    figurines->insert(new Figurine(PlayerType::Computer, FigurineType::Bomb));
}

void MainWindow::initializeMusic()
{
//    playlistsound = new QMediaPlaylist();
//    playlistsound->addMedia(QUrl("qrc:/sounds/Sounds/sound.mp3"));
//    playlistsound->setPlaybackMode(QMediaPlaylist::Loop);

//    playlisteasy = new QMediaPlaylist();
//    playlisteasy->addMedia(QUrl("qrc:/sounds/Sounds/easy_play_song.mp3"));
//    playlisteasy->setPlaybackMode(QMediaPlaylist::Loop);

//    playlistmedium = new QMediaPlaylist();
//    playlistmedium->addMedia(QUrl("qrc:/sounds/Sounds/medium_play_song.mp3"));
//    playlistmedium->setPlaybackMode(QMediaPlaylist::Loop);

//    playlisthard = new QMediaPlaylist();
//    playlisthard->addMedia(QUrl("qrc:/sounds/Sounds/hard_play_song.mp3"));
//    playlisthard->setPlaybackMode(QMediaPlaylist::Loop);

//    player = new QMediaPlayer(this);
//    player->setPlaylist(playlistsound);
//    player->setMedia(QUrl("qrc:/sounds/Sounds/sound.mp3"));
//    player->play();

//    connect(ui->horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT(setVolume(int)));
//    connect(ui->horizontalSlider, SIGNAL(valueChanged(int)), player, SLOT(setVolume(int)));

//    ui->pbOnOff->setIcon(QIcon(":/icons/Icons/on.jpg"));
//    ui->pbOnOff->setCheckable(true);

//    medium_game_song = new QMediaPlayer(this);
//    medium_game_song->setMedia(QUrl("qrc:/sounds/Sounds/medium_play_song.mp3"));
//    medium_game_song->setPlaylist(playlistmedium);

//    easy_game_song = new QMediaPlayer(this);
//    easy_game_song->setMedia(QUrl("qrc:/sounds/Sounds/easy_play_song.mp3"));
//    easy_game_song->setPlaylist(playlisteasy);

//    hard_game_song = new QMediaPlayer(this);
//    hard_game_song->setMedia(QUrl("qrc:/sounds/Sounds/hard_play_song.mp3"));
//    hard_game_song->setPlaylist(playlisthard);

//    buttons_click2 = new QMediaPlayer(this);
//    buttons_click2->setMedia(QUrl("qrc:/sounds/Sounds/button_click2.mp3"));

//    on_off_click = new QMediaPlayer(this);
//    on_off_click->setMedia(QUrl("qrc:/sounds/Sounds/on_off_click.mp3"));
//    on_off_click->setVolume(8);

//    radio_button_click = new QMediaPlayer(this);
//    radio_button_click->setMedia(QUrl("qrc:/sounds/Sounds/radio_button_click.mp3"));
}
