#include "../Headers/tablefigurinescene.h"
#include <iostream>
#include <vector>
#include "../Headers/figurine.h"
#include <QGraphicsScene>
#include <QPoint>
#include <QMouseEvent>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>
#include "../Headers/tablesetupscene.h"
#include "../Headers/field.h"
#include "../Headers/mainwindow.h"

TableFigurineScene::TableFigurineScene(QObject *o)
{
    this->setParent(o);
    m_clickedFigurine = nullptr;
    m_otherScene = nullptr;
    m_view = nullptr;
    m_board = nullptr;
    status = SelectingStatus::Neutral;
}

TableFigurineScene::~TableFigurineScene()
{
//    delete m_clickedFigurine;
//    delete m_otherScene;
//    delete m_view;
//    delete m_board;
}

void TableFigurineScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent){
    if (mouseEvent->button() == Qt::LeftButton)
    {
        QGraphicsItem *item = itemAt(mouseEvent->scenePos(), QTransform());
        // Already selected figurine
        if(m_clickedFigurine){
            m_clickedFigurine->setSelected(false);
            this->update(m_clickedFigurine->getXPos(), m_clickedFigurine->getYPos(),
                         m_clickedFigurine->getWidth(), m_clickedFigurine->getHeight());
        }
        // Select new figurine
        m_clickedFigurine = qgraphicsitem_cast<Figurine *>(item);
        if(m_clickedFigurine)
        {
            m_clickedFigurine->setSelected(true);
            m_otherScene->SetClickedFigurine(m_clickedFigurine);
            status = SelectingStatus::SelectedPicked;
            this->update(m_clickedFigurine->getXPos(), m_clickedFigurine->getYPos(),
                         m_clickedFigurine->getWidth(), m_clickedFigurine->getHeight());
        }
    }
}

void TableFigurineScene::setOtherScene(TableSetupScene *other){
    m_otherScene = other;
}

void TableFigurineScene::setView(QGraphicsView* view){
    m_view = view;
}

Figurine* TableFigurineScene::getClickedFigurine(){
    return m_clickedFigurine;
}

void TableFigurineScene::SetClickedFigurine(Figurine *f){
    m_clickedFigurine = f;
}

Board* TableFigurineScene::getBoard(){
    return m_board;
}

void TableFigurineScene::setBoard(Board *board){
    m_board = board;
}
