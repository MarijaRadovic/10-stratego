#include "../Headers/gamescene.h"
#include <iostream>
#include <vector>
#include "../Headers/figurine.h"
#include <QGraphicsScene>
#include <QPoint>
#include <QMouseEvent>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>
#include "../Headers/tablesetupscene.h"
#include "../Headers/field.h"
#include "../Headers/dialog.h"
#include <thread>

bool selected = false;
int oI, oJ;

void playSound(GameScene *gs){
    Q_UNUSED(gs)
//    gs->getStepSound()->setVolume(50);
//    gs->getStepSound()->play();
}

void GameScene::moveFigurine(int oldI, int oldJ, int newI, int newJ, Board *board, int sqSize){
    m_game->updateScoreLabel();

    Figurine* fig = (*board->getPboard())[oldI][oldJ]->getFigurine();
    Figurine* enemy = (*board->getPboard())[newI][newJ]->getFigurine();

    if(!enemy){
//        if(fig->getOwner() == PlayerType::Computer){
//            qse2.setVolume(50);
//            qse2.play();
//            qse2.stop();
//        }
//        else if(fig->getOwner() == PlayerType::Human){
//            qse.setVolume(50);
//            qse.play();
//            qse.stop();
//        }

        fig->setOldI(oldI);
        fig->setOldJ(oldJ);
        int forUpdateX = fig->getXPos();
        int forUpdateY = fig->getYPos();
        this->removeItem((*board->getPboard())[oldI][oldJ]->getFigurine());
        (*board->getPboard())[oldI][oldJ]->setFigurine(nullptr);
        (*board->getPboard())[oldI][oldJ]->setFieldType(FieldType::Free);
        this->update(forUpdateX, forUpdateY, sqSize, sqSize);
        this->addItem(fig);
        int newXPos = newJ * sqSize;
        int newYPos = newI * sqSize;
        fig->setXPos(newXPos);
        fig->setYPos(newYPos);
        (*board->getPboard())[newI][newJ]->setFigurine(fig);
        forUpdateX = fig->getXPos();
        forUpdateY = fig->getYPos();
        this->update(forUpdateX, forUpdateY, sqSize, sqSize);
    }
    else{

        m_game->getNotification()->setText("BATTLE!");

        if(enemy->getOwner() != fig->getOwner()){
            fig->setInBattle(InBattle::attack);
            enemy->setInBattle(InBattle::defend);
            this->update(fig->getXPos(), fig->getYPos(), m_squareSize, m_squareSize);
            this->update(enemy->getXPos(), enemy->getYPos(), m_squareSize, m_squareSize);
            Dialog::showBattle(fig, enemy);
            fig->setInBattle(InBattle::notInBattle);
            enemy->setInBattle(InBattle::notInBattle);

            if(*fig > *enemy){
                int forUpdateXEnemy = sqSize*newJ;
                int forUpdateYEnemy = sqSize*newI;
                this->removeItem((*m_board->getPboard())[newI][newJ]->getFigurine());
                (*m_board->getPboard())[newI][newJ]->setFigurine(nullptr);
                this->update(forUpdateXEnemy, forUpdateYEnemy, sqSize, sqSize);
                (*m_board->getPboard())[newI][newJ]->setEnemy(false);

                int forUpdateX = sqSize*oldJ;
                int forUpdateY = sqSize*oldI;
                this->removeItem((*m_board->getPboard())[oldI][oldJ]->getFigurine());
                (*m_board->getPboard())[oldI][oldJ]->setFigurine(nullptr);
                this->update(forUpdateX, forUpdateY, sqSize, sqSize);
                this->addItem(fig);
                fig->setXPos(newJ*sqSize);
                fig->setYPos(newI*sqSize);

                fig->setOldI(oldI);
                fig->setOldJ(oldJ);

                (*m_board->getPboard())[newI][newJ]->setFigurine(fig);
                forUpdateX = fig->getXPos();
                forUpdateY = fig->getYPos();
                this->update(forUpdateX, forUpdateY, sqSize, sqSize);
            }
            else if ((*fig) == (*enemy)){
                int forUpdateXfig = sqSize*oldJ;
                int forUpdateYfig = sqSize*oldI;
                this->removeItem((*m_board->getPboard())[oldI][oldJ]->getFigurine());
                (*m_board->getPboard())[oldI][oldJ]->setFigurine(nullptr);
                this->update(forUpdateXfig, forUpdateYfig, sqSize, sqSize);

                int forUpdateXEnemy = sqSize*newJ;
                int forUpdateYEnemy = sqSize*newI;
                this->removeItem((*m_board->getPboard())[newI][newJ]->getFigurine());
                (*m_board->getPboard())[newI][newJ]->setFigurine(nullptr);

                (*m_board->getPboard())[newI][newJ]->setAvailable(false);
                (*m_board->getPboard())[newI][newJ]->setEnemy(false);
                (*m_board->getPboard())[newI][newJ]->setFieldType(FieldType::Free);
                this->update(forUpdateXEnemy, forUpdateYEnemy, sqSize, sqSize);
            }
            else {
                int forUpdateXfig = sqSize*oldJ;
                int forUpdateYfig = sqSize*oldI;
                this->removeItem((*m_board->getPboard())[oldI][oldJ]->getFigurine());
                (*m_board->getPboard())[oldI][oldJ]->setFigurine(nullptr);
                this->update(forUpdateXfig, forUpdateYfig, sqSize, sqSize);
            }            
        }
    }
}

//QMediaPlayer *GameScene::getStepSound() const
//{
//    //return stepSound;
//}

//void GameScene::setStepSound(QMediaPlayer *value)
//{
//    Q_UNUSED(value)
//   // stepSound = value;
//}


GameScene::GameScene(QObject *o)
{
    this->setParent(o);
    m_clickedFigurine = nullptr;
    m_view = nullptr;
    m_board = nullptr;
    m_game = nullptr;
    shouldWaitEvents = true;
    m_squareSize = 90;

    //qse.setSource(QUrl("qrc:/sounds/Sounds/step.wav"));

    //qse2.setSource(QUrl("qrc:/sounds/Sounds/step.wav"));
}

GameScene::~GameScene()
{
//    delete stepSound;
//    delete qse;
//    delete qse2;
//    delete m_clickedFigurine;
//    delete m_view;
//    delete m_board;
//    delete m_game;
}

void GameScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent){

    if(m_game->getTurn() != Turn::Human || getGame()->getGameEnded()){
        return;
    }

    if (mouseEvent->button() == Qt::LeftButton)
    {
        int j = ((mouseEvent->scenePos().x()) / 90);
        int i = ((mouseEvent->scenePos().y()) / 90);
        if(i<0 || i>9 || j<0 || j>9)
            return;
        Figurine *selectedFig = (*m_board->getPboard())[i][j]->getFigurine();
        if(selectedFig && selectedFig->getOwner() == PlayerType::Human){
            if(selected)
                resetAvailableFields();

            selectedFig->whereCanMove(i, j, m_board);
            oI = i;
            oJ = j;

            selected = true;
            return;
        }
        if(selected  && (*m_board->getPboard())[i][j]->getAvailable()){
            resetAvailableFields();
            moveFigurine(oI, oJ, i, j, m_board, m_squareSize);

            selected = false;
            m_game->setTurn(Turn::Computer);
            m_game->getNotification()->setText("Opponent is thinking... \n You have to wait!");

            QWidget *viewport = m_view->viewport();
            viewport->repaint();

            return m_game->playGame();
        }
    }
}

QGraphicsView *GameScene::getView(){
    return m_view;
}

bool GameScene::getShouldWaitEvents() const{
    return shouldWaitEvents;
}

void GameScene::setShouldWaitEvents(bool value){
    shouldWaitEvents = value;
}

void GameScene::resetAvailableFields(){
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10; j++){
            if((*m_board->getPboard())[i][j]->getAvailable()){
                (*m_board->getPboard())[i][j]->setAvailable(false);
                (*m_board->getPboard())[i][j]->setEnemy(false);
                int forUpdateX = (*m_board->getPboard())[i][j]->getXPos();
                int forUpdateY = (*m_board->getPboard())[i][j]->getYPos();
                this->update(forUpdateX, forUpdateY, 90, 90);
            }
        }
    }
}

void GameScene::setView(QGraphicsView* view){
    m_view = view;
}

Figurine* GameScene::getClickedFigurine(){
    return m_clickedFigurine;
}

void GameScene::SetClickedFigurine(Figurine *f){
    m_clickedFigurine = f;
}

Board *GameScene::getBoard(){
    return m_board;
}

void GameScene::setBoard(Board* board){
    m_board = board;
}

Game* GameScene::getGame(){
    return m_game;
}

void GameScene::setGame(Game* game){
    m_game = game;
}
