#ifndef FIELD_H
#define FIELD_H

class Game;
#include "figurine.h"
#include <QGraphicsItem>
#include <QPen>
#include <QPointF>
#include <QRectF>
#include <QWidget>

class Field : public QGraphicsItem {

private:
  FieldType m_fieldType;
  Figurine *m_figurine;
  Figurine m_fig;

  int m_xPos, m_yPos, m_width, m_height;
  bool m_available;
  bool m_enemy;
  int m_specialFieldFlag;

public:
  ~Field();

  Field( FieldType ft, int x, int y );
  Field( FieldType ft );
  Field( const Field &other );

  FieldType getFieldType() const;
  Figurine *getFigurine() const;

  void setFieldType( const FieldType &fieldType );
  void setFigurine( Figurine *figurine );

  bool canBeOccupied();
  bool isOccupied();
  bool isForbidden();

  //    void moveFigurine(int, int, FieldType, Figurine*, State);


  QRectF boundingRect() const override;
  void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget ) override;


  Field operator=( Field other );

  int getXPos() const;
  void setXPos( int xPos );
  int getYPos() const;
  void setYPos( int yPos );
  int getWidth() const;
  void setWidth( int width );
  int getHeight() const;
  void setHeight( int height );
  bool getAvailable() const;
  void setAvailable( bool available );
  int getSpecialFieldFlag() const;
  void setSpecialFieldFlag( int value );
  bool getEnemy() const;
  void setEnemy( bool enemy );
  Figurine &getFig();
  void setFig( Figurine fig );
};

#endif // FIELD_H
