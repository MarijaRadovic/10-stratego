#ifndef TABLESETUPSCENE_H
#define TABLESETUPSCENE_H

#include "board.h"
#include "field.h"
#include "figurine.h"
#include "tablefigurinescene.h"
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QMouseEvent>
#include <QPoint>
#include <vector>

class TableFigurineScene;

class TableSetupScene : public QGraphicsScene {
  Q_OBJECT
public:
  explicit TableSetupScene( QObject *parent = 0 );
  ~TableSetupScene();
  void setOtherScene( TableFigurineScene * );
  void setView( QGraphicsView * );
  void SetClickedFigurine( Figurine * );
  Figurine *getClickedFigurine();
  Board *getBoard();
  void setBoard( Board *board );

  QGraphicsView *getView() const;

protected:
  void mousePressEvent( QGraphicsSceneMouseEvent *mouseEvent );

private:
  Figurine *m_clickedFigurine;
  TableFigurineScene *m_otherScene;
  QGraphicsView *m_view;
  Board *m_board;
  int m_squareSize;
};

#endif // TABLESETUPSCENE_H
