#ifndef FIGURINETYPE_H
#define FIGURINETYPE_H

enum class FigurineType {
  Bomb,
  Marshal,
  General,
  Colonel,
  Major,
  Captain,
  Lieutenant,
  Sergeant,
  Miner,
  Scout,
  Spy,
  Flag,
  None
};

#endif // FIGURINETYPE_H
