#ifndef FIGURINE_H
#define FIGURINE_H

#include "enums.h"
#include "figurinetype.h"

#include <QGraphicsItem>
#include <QPainter>
#include <QRectF>
#include <QString>
#include <QWidget>
#include <iostream>
class Board;


class Figurine : public QGraphicsItem {

private: // members
  int m_xPos, m_yPos, m_width, m_height, m_strength;
  PlayerType m_owner;
  FigurineType m_figurineType;
  bool m_isSelected, m_placed, m_crossed;
  int m_oldI;
  int m_oldJ;
  InBattle m_inBattle;

public: // methods
  ~Figurine();

  /***CONSTRUCTORS***/
  Figurine( int x, int y, PlayerType owner, FigurineType figurineType );
  Figurine( PlayerType owner, FigurineType figurineType );
  Figurine( PlayerType owner, int strength );
  Figurine( const Figurine &other );
  Figurine();

  Figurine operator=( Figurine other );

  /***METHODS FROM QGRAPHICSITEM***/
  QRectF boundingRect() const override;
  void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget ) override;

  /***OPERATORS***/
  bool operator>( const Figurine &other ) const;
  bool operator==( const Figurine &other ) const;
  bool operator<( const Figurine &other ) const;

  /***AUXILIARY METHODS***/
  int strengthFromFigurineType( FigurineType );
  FigurineType figurineTypeFromStrength( int );
  void whereCanMove( int i, int j, Board *board );
  QString getImageSource( PlayerType pt, FigurineType ft, int s );

  /***GETTERS***/
  int getXPos() const;
  int getYPos() const;
  int getWidth() const;
  int getHeight() const;
  int getStrength() const;
  PlayerType getOwner() const;
  FigurineType getFigurineType() const;
  bool getIsSelected() const;
  int getOldI() const;
  int getOldJ() const;
  InBattle getInBattle() const;

  /***SETTERS***/
  void setXPos( int );
  void setYPos( int );
  void setWidth( int );
  void setHeight( int );
  void setStrength( int );
  void setOwner( PlayerType );
  void setFigurineType( FigurineType );
  void setSelected( bool value );
  void setSize( int );
  void setOldI( int oldI );
  void setOldJ( int oldJ );
  void setInBattle( InBattle inBattle );
  bool getCrossed() const;
  void setCrossed( bool crossed );
};

#endif // FIGURINE_H
