#ifndef GAMESCENE_H
#define GAMESCENE_H

#include "field.h"

#include "game.h"
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>

#include <QMouseEvent>
#include <QPoint>
#include <vector>

class Game;

class GameScene : public QGraphicsScene {
  Q_OBJECT
public:
  explicit GameScene( QObject *parent = 0 );

  ~GameScene();
  void setView( QGraphicsView * );
  void SetClickedFigurine( Figurine * );
  Figurine *getClickedFigurine();
  Board *getBoard();
  void setBoard( Board *board );
  Game *getGame();
  void setGame( Game * );
  void resetAvailableFields();

  bool getShouldWaitEvents() const;
  void setShouldWaitEvents( bool value );

  QGraphicsView *getView();
  void mousePressEvent( QGraphicsSceneMouseEvent *mouseEvent );

  void moveFigurine( int oldI, int oldJ, int newI, int newJ, Board *board, int sqSize );

//  QMediaPlayer *getStepSound() const;
//  void setStepSound( QMediaPlayer *value );

private:
//  QMediaPlayer *stepSound;
//  QSoundEffect qse;
//  QSoundEffect qse2;
  Figurine *m_clickedFigurine;
  QGraphicsView *m_view;
  Board *m_board;
  Game *m_game;
  bool shouldWaitEvents;
  int m_squareSize;
};

#endif // GAMESCENE_H
