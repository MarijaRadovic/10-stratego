#ifndef ENUMS_H
#define ENUMS_H

enum class PlayerType { Computer, Human };

enum class FieldType { Free, Occupied, Forbidden };

enum class Turn { Human, Computer };

enum class SelectingStatus { Neutral, SelectedPicked, SelectedForMoving, SelectedMoved };

enum class InBattle { attack, defend, notInBattle };

enum class GameDifficulty { EASY, MEDIUM, HARD };

#endif // ENUMS_H
