#ifndef BOARD_H
#define BOARD_H

#include <vector>

#include "field.h"
class Field;

typedef std::vector<std::vector<Field>> State;
typedef std::vector<std::vector<Field *>> *pState;

class Board {

private:
  State m_board;
  pState m_pboard;
  bool firstRowFree;
  bool allFigurinesSet;
  bool bombsNotBlockingWay;

public:
  ~Board();

  Board();
  Board( Board &other );
  Board( State );

  State &getBoard();
  void setBoard( const State &board );
  pState getPboard() const;
  void setPboard( const pState &pboard );
  void fromPointerToState();
  void fromStateToPointer();
  void setField( int, int, State );
  void setField( int, int, pState );
  bool checkPlayerSetup();


  bool getFirstRowFree() const;
  bool getAllFigurinesSet() const;
  bool getBombsNotBlockingWay() const;
};

#endif // BOARD_H
